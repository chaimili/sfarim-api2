
val ktor_version: String by project
val kotlin_version: String by project
val logback_version: String by project

plugins {
	id("com.google.cloud.tools.jib") version "2.6.0"
    application
    kotlin("jvm") version "1.3.72"
    kotlin("kapt") version "1.3.72"
}

group = "sfarim-api"
version = "0.0.1"

application {
	mainClassName = "com.lomdaat.ApplicationKt"
}

jib {
	container.mainClass = "com.lomdaat.ApplicationKt"
}

kotlin.sourceSets["main"].kotlin.srcDirs("src")
sourceSets["main"].resources.srcDirs("resources")

kotlin.sourceSets["test"].kotlin.srcDirs("test")
sourceSets["test"].resources.srcDirs("testresources")

repositories {
	jcenter()
	mavenCentral()
//	maven { url 'https://kotlin.bintray.com/ktor' }
	maven { url = uri("https://dl.bintray.com/kotlin/exposed") }
	maven { url = uri("https://kotlin.bintray.com/kotlinx") }
	maven { url = uri("https://dl.bintray.com/kodein-framework/Kodein-DI/") }
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:$kotlin_version")
    implementation("io.ktor:ktor-server-netty:$ktor_version")
    implementation("ch.qos.logback:logback-classic:$logback_version")
    implementation("io.ktor:ktor-server-core:$ktor_version")
    implementation("io.ktor:ktor-locations:$ktor_version")
    implementation("io.ktor:ktor-server-host-common:$ktor_version")
    //implementation "io.ktor:ktor-auth:$ktor_version"
    implementation("io.ktor:ktor-gson:$ktor_version")
    implementation("io.ktor:ktor-locations:$ktor_version")

    //ktor-client
    implementation("io.ktor:ktor-client-core:$ktor_version")
    implementation("io.ktor:ktor-client-apache:$ktor_version")
    implementation("io.ktor:ktor-client-json:$ktor_version")
    implementation("io.ktor:ktor-client-json-jvm:$ktor_version")
    implementation("org.apache.xmlrpc:xmlrpc-client:3.1.3")
    //implementation "io.ktor:ktor-auth:$ktor_version"
    implementation("io.ktor:ktor-auth-jwt:$ktor_version")
    testImplementation("io.ktor:ktor-server-test-host:$ktor_version")
	testImplementation("io.ktor:ktor-server-tests:$ktor_version")
	testImplementation("io.ktor:ktor-server-core:$ktor_version")
	testImplementation("io.ktor:ktor-server-host-common:$ktor_version")
	testImplementation("io.ktor:ktor-locations:$ktor_version")
	testImplementation("io.ktor:ktor-auth:$ktor_version")
	testImplementation("io.ktor:ktor-gson:$ktor_version")

    //di
    implementation("org.kodein.di:kodein-di-generic-jvm:6.2.0")

    // Json
    implementation("com.github.salomonbrys.kotson:kotson:2.5.0")

    //moshi
    implementation("com.squareup.moshi:moshi-kotlin:1.6.0")

    //logging
    implementation("io.github.microutils:kotlin-logging:1.5.6")

    // s3 store
    implementation("com.amazonaws:aws-java-sdk-s3:1.11.257")

    //exposed
    implementation("org.jetbrains.exposed:exposed:0.10.5")

    //postgresql

    //	compile 'org.postgresql:postgresql:42.1.1'

    //jasync-sql
    implementation("com.github.jasync-sql:jasync-postgresql:1.0.12")

    //coroutines
    val coroutinesVersion = "1.3.5"

    // TODO: update version
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutinesVersion")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-jdk8:$coroutinesVersion")

    //simple java mail
    implementation("org.simplejavamail:simple-java-mail:5.0.0")
    implementation("taglibs:mailer:1.1")
    implementation("net.markenwerk:utils-mail-dkim:1.1.12")

    //pdfbox
    implementation("org.apache.pdfbox:pdfbox:2.0.21")

    //micronaut
    val micronautVersion= "1.3.4"
    kapt("io.micronaut:micronaut-inject-java:$micronautVersion")
    implementation("io.micronaut:micronaut-inject:$micronautVersion")
    implementation("io.micronaut:micronaut-http-client:$micronautVersion")

}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf(
			"-XXLanguage:+InlineClasses",
			"-Xuse-experimental=io.ktor.util.KtorExperimentalAPI"
		)

		jvmTarget = "1.8"
	}
}
