import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import org.junit.Test
import util.*

data class TestData(
        @OdooFalseString
        val name: String
)
data class TestDataInt(
        @OdooFalseInt
        val zip: Int
)

data class TestDataDouble(
        @OdooFalseDouble
        val id: Double
)

data class TestDataDate(
        @OdooFalseDate
        val zip: Int
)

data class TestDataIds(
        @OdooIdsParser
        val id: Int
)

data class TestDataSecondIds(
        @OdooIdsSecondParser
        val id: String
)

data class TestDataObjectToList(
        @OdooObjectToList
        val list: List<*>
)

class JsonParserTest {
    @Test
    fun testEmptyStringParser(){
        val moshi = Moshi.Builder().add(StringAdapter()).add(KotlinJsonAdapterFactory()).build()
        val nullJson = """
            { "name": false }
        """.trimIndent()

        val result = moshi.adapter<TestData>(TestData::class.java).fromJson(nullJson)!!

        assert(TestData("") == result)
    }

    @Test
    fun testWithStringStringParser(){
        val moshi = Moshi.Builder().add(StringAdapter()).add(KotlinJsonAdapterFactory()).build()
        val stringJson = """
            { "name": "Hello" }
        """.trimIndent()

        val result = moshi.adapter<TestData>(TestData::class.java).fromJson(stringJson)!!

        assert(TestData("Hello") == result)
    }

    @Test
    fun testOdooFalseInt(){
        val moshi = Moshi.Builder().add(IntAdapter()).add(KotlinJsonAdapterFactory()).build()
        val nullJson = """
            { "zip": false }
        """.trimIndent()

        val result = moshi.adapter<TestDataInt>(TestDataInt::class.java).fromJson(nullJson)!!
        assert(TestDataInt(0) == result)
    }

    @Test
    fun testWithValueOdooFalseInt(){
        val moshi = Moshi.Builder().add(IntAdapter()).add(KotlinJsonAdapterFactory()).build()
        val nullJson = """
            { "zip": 123 }
        """.trimIndent()

        val result = moshi.adapter<TestDataInt>(TestDataInt::class.java).fromJson(nullJson)!!
        assert(TestDataInt(123) == result)
    }

    @Test
    fun testOdooFalseDouble(){
        val moshi = Moshi.Builder().add(DoubleAdapter()).add(KotlinJsonAdapterFactory()).build()
        val nullJson = """
            { "id": 123.0 }
        """.trimIndent()

        val result = moshi.adapter<TestDataDouble>(TestDataDouble::class.java).fromJson(nullJson)!!
        assert(TestDataDouble(123.0) == result)
    }

//    @Test
//    fun testOdooFalseDate(){
//        val moshi = Moshi.Builder().add(DateAdapter()).add(KotlinJsonAdapterFactory()).build()
//        val nullJson = """
//            { "zip": false }
//        """.trimIndent()
//
//        val result = moshi.adapter<TestDataDate>(TestDataDate::class.java).fromJson(nullJson)!!
//        assert(TestDataDate(0) == result)
//    }

    @Test
    fun testIdAdapterWithValue(){
        val moshi = Moshi.Builder().add(IdAdapter()).add(KotlinJsonAdapterFactory()).build()
        val listJson = """
            { "id": [123.0, ""] }
        """.trimIndent()

        val result = moshi.adapter<TestDataIds>(TestDataIds::class.java).fromJson(listJson)!!
        assert(TestDataIds(123) == result)
    }

    @Test
    fun testIdSecondAdapterWithValue(){
        val moshi = Moshi.Builder().add(IdSecondAdapter()).add(KotlinJsonAdapterFactory()).build()
        val listJson = """
            { "id": [123.0, "hi"] }
        """.trimIndent()

        val result = moshi.adapter<TestDataSecondIds>(TestDataSecondIds::class.java).fromJson(listJson)!!
        assert(TestDataSecondIds("hi").equals(result))
    }

    @Test
    fun testIdAdapter(){
        val moshi = Moshi.Builder().add(IdAdapter()).add(KotlinJsonAdapterFactory()).build()
        val listJson = """
            { "id": false }
        """.trimIndent()

        val result = moshi.adapter<TestDataIds>(TestDataIds::class.java).fromJson(listJson)!!
        assert(TestDataIds(0) == result)
    }

    @Test
    fun testIdAdapterWithFalseId(){
        val moshi = Moshi.Builder().add(IdAdapter()).add(KotlinJsonAdapterFactory()).build()
        val listJson = """
            { "id": [false, ""] }
        """.trimIndent()

        val result = moshi.adapter<TestDataIds>(TestDataIds::class.java).fromJson(listJson)!!
        assert(TestDataIds(0) == result)
    }

//    @Test
//    fun testObjectToListAdapter(){
//        val moshi = Moshi.Builder().add(BeginObjectAdapter()).add(KotlinJsonAdapterFactory()).build()
//        val listJson = """
//            {"list": [1], "id": 8527}
//        """.trimIndent()
//        val result = moshi.adapter<TestDataObjectToList>(TestDataObjectToList::class.java).fromJson(listJson)!!
//        assert(TestDataObjectToList(listOf(listOf(1), 8527)).list.equals(result))
//    }


}