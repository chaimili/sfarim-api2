package handleFile

import com.lomdaat.logger
import com.lomdaat.odooResponses.PreviewPages
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import odooResponses.repositories.impl.tempPath
import org.apache.pdfbox.io.MemoryUsageSetting
import org.apache.pdfbox.pdmodel.PDDocument
import org.apache.pdfbox.multipdf.Splitter
import java.io.*
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

class HandleFile {

    suspend fun splitFile(
        file: File,
        fileName: String,
        encrypt: suspend (File, Int) -> Unit
    ): Pair<File, Int> = withContext(Dispatchers.IO) {

        logger.debug { "SplitFile started" }

        val document = PDDocument.load(file, MemoryUsageSetting.setupTempFileOnly())

        document.splitChunks(50).forEach {
            it.second.splitToPages(fileName, File(tempPath), it.first).forEach { page ->
                encrypt(page.first, page.second)
            }
        }

        logger.debug { "SplitFile document:$document" }

        return@withContext Pair(File(tempPath, fileName), document.numberOfPages).also {
            logger.debug { "SplitFile pair:$it" }
        }
    }

    private fun PDDocument.splitChunks(chunkSize: Int): Sequence<Pair<Int, PDDocument>> = sequence {
        use { document ->
            val splitter = Splitter().apply {
                setSplitAtPage(chunkSize)
            }

            splitter.split(document).mapIndexed { i, currChunk ->
                currChunk.use {
                    yield((i * chunkSize) to it)
                }
            }
        }
    }

    private fun PDDocument.splitToPages(fileName: String, outputFolder: File, startPage: Int): List<Pair<File, Int>> =
        let { document ->
            Splitter().split(document).mapIndexed { index, pdDocument ->
                val file = File.createTempFile(fileName, "-${startPage + index + 1}", outputFolder)
                pdDocument.use {
                    it.save(file)
                }
                Pair(file, startPage + index + 1)
            }
        }

    suspend fun zipFolder(srcFolder: String, destZipFile: String): File = withContext(Dispatchers.IO) {
        val fileWriter = FileOutputStream(destZipFile)
        val zip = ZipOutputStream(fileWriter)

        logger.debug { "zipFolder file:$fileWriter, zip:$zip, source:$srcFolder" }
        addFolderToZip("", srcFolder, zip)

        zip.flush()
        zip.close()

        if (File(destZipFile).exists())
            File(srcFolder).deleteRecursively()

        return@withContext File(destZipFile)
    }

    private suspend fun addFileToZip(path: String, srcFile: String, zip: ZipOutputStream) =
        withContext(Dispatchers.IO) {
            val folder = File(srcFile)
            if (folder.isDirectory) {
                addFolderToZip(path, srcFile, zip)
            } else {
                val buffer = ByteArray(1024)
                val input = FileInputStream(srcFile)
                var length = input.read(buffer)

                zip.putNextEntry(ZipEntry(folder.name))

                while (length > 0) {
                    zip.write(buffer, 0, length)
                    length = input.read(buffer)
                }
            }
        }

    private suspend fun addFolderToZip(path: String, srcFolder: String, zip: ZipOutputStream) {
        val folder = File(srcFolder)

        logger.debug { "FolderToZip folder:$folder" }

        folder.list()?.forEach { fileName ->
            if (path == "") {
                addFileToZip(folder.name, "$srcFolder/$fileName", zip)
            } else {
                addFileToZip(path + "/" + folder.name, "$srcFolder/$fileName", zip)
            }
        }
    }


    suspend fun splitToPreview(
        file: File,
        fileName: String,
        pages: PreviewPages
    ): File = withContext(Dispatchers.IO) {

        val document = PDDocument.load(file).also {
            logger.debug { "SplitPreview document:$it" }
        }

        val previewFile = File(tempPath, "$fileName.pdf")

        document.use { doc ->
            val splitter = Splitter().apply {
                setStartPage(pages.startPage)
                setEndPage(pages.endPage)
                setSplitAtPage(pages.endPage - pages.startPage + 1)
            }

            splitter.split(doc).first { pdDocument ->
                pdDocument.use {
                    it.save(previewFile)
                }
                true
            }.also {
                logger.debug { "SplitPreview pages:${it.pages.count}" }
            }
        }
        return@withContext previewFile
    }
}

