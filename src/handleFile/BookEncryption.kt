package handleFile

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.*
import java.security.MessageDigest
import javax.crypto.Cipher
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.PBEKeySpec
import javax.crypto.spec.SecretKeySpec
import java.security.NoSuchAlgorithmException
import javax.crypto.CipherOutputStream

class BookEncryption {
    suspend fun encrypt(file: File, fileName: String, dirName: String): Boolean = withContext(Dispatchers.IO) {
        if (file.length() == 0L && !file.exists())
            throw NullPointerException("must provide input file")

        val name = if (dirName.isBlank()) fileName else dirName
        val cipher = getCipher(name, Cipher.ENCRYPT_MODE)

        val fis = FileInputStream(file)
        val encryptedDir = File(file.parentFile, dirName)

        if (!encryptedDir.exists() && !dirName.isBlank())
            encryptedDir.mkdir()

        val encryptedFile = File(encryptedDir, fileName)
        val fos = FileOutputStream(encryptedFile)
        val output = CipherOutputStream(fos, cipher)

        val plainText = ByteArray(4096)
        var bytesRead = fis.read(plainText)

        while (bytesRead >= 0) {
            output.write(plainText, 0, bytesRead)
            bytesRead = fis.read(plainText)
        }
        try {
            output.flush()
            output.close()
            fos.close()
            fis.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return@withContext if (encryptedFile.exists() && encryptedFile.length() > 0) {
            file.deleteRecursively()
            true
        } else false
    }

    private fun getCipher(fileName: String, mode: Int): Cipher {
        val iv = "D46EC2E02BDF37DBC1F7E27ECE41A255"
        val salt = "7883C1F470C915E4"
        val pwd = MD5(fileName)
        val factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1")
        val spec = PBEKeySpec(pwd!!.toCharArray(), salt.toByteArray(), 1024, 256)
        val tmp = factory.generateSecret(spec)
        val secret = SecretKeySpec(tmp.encoded, "AES")

        val ivBytes = hex2bin(iv)
        val ivSpec = IvParameterSpec(ivBytes)

        val cipher = Cipher.getInstance("/AES/CBC/NoPadding")
        cipher.init(mode, secret, ivSpec)

        return cipher
    }

    private fun MD5(string: String): String? {

        var hash: String? = null
        val message: ByteArray
        try {
            message = string.toByteArray(charset("UTF-8"))
        } catch (e: UnsupportedEncodingException) {
            return hash
        }

        val md: MessageDigest
        try {
            md = MessageDigest.getInstance("MD5")
            md.update(message)

            hash = toHexString(md.digest())
        } catch (e: NoSuchAlgorithmException) {
            return hash
        }

        return hash

    }

    private fun toHexString(bytes: ByteArray?): String? {
        var hexString: String? = null

        if (bytes == null)
            return hexString
        val sb = StringBuffer()
        for (i in bytes.indices) {
            val unsigned = bytes[i].toInt() and 0xff
            if (unsigned < 0x10)
                sb.append("0")
            sb.append(Integer.toHexString(unsigned))
        }
        hexString = sb.toString()
        return hexString
    }

    private fun hex2bin(str: String?): ByteArray? {
        return when {
            str == null -> null
            str.length < 2 -> null
            else -> {
                val len = str.length / 2
                val buffer = ByteArray(len)
                for (i in 0 until len) {
                    buffer[i] = Integer.parseInt(str.substring(i * 2, i * 2 + 2), 16).toByte()
                }
                buffer
            }
        }
    }
}
