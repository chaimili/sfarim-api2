package com.lomdaat.handleFile.s3

import com.amazonaws.AmazonServiceException
import com.amazonaws.SdkClientException
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.model.*
import kotlinx.coroutines.withContext
import java.io.InputStream
import kotlinx.coroutines.Dispatchers
import odooResponses.repositories.impl.tempPath
import org.slf4j.LoggerFactory
import java.io.File
import java.net.URL

class S3ObjectStorage(
        private val s3Client: AmazonS3,
        private val bucketName: String
) {

    val logger = LoggerFactory.getLogger(S3ObjectStorage:: class.java)
    private fun PutObjectRequest.toS3PutObjectRequest(isPublic: Boolean): com.amazonaws.services.s3.model.PutObjectRequest {

        val result = com.amazonaws.services.s3.model.PutObjectRequest(
                bucketName,
                key,
                input,
                ObjectMetadata().also { objectMetadata ->
                    objectMetadata.contentLength = length
                    objectMetadata.contentType = type.typeName

                    userMeta.forEach {
                        objectMetadata.addUserMetadata(it.key, it.value)
                    }
                }
        )
        if (isPublic)
            result.withCannedAcl(CannedAccessControlList.PublicRead)

        return result
    }

    suspend fun putObject(putObjectRequest: PutObjectRequest, isPublic: Boolean) = withContext(Dispatchers.IO) {
        s3Client.putObject(putObjectRequest.toS3PutObjectRequest(isPublic))

        Unit
    }

    suspend fun deleteObject(key: String) = withContext(Dispatchers.IO) {
        s3Client.deleteObject(bucketName, key)

        Unit
    }

    fun listObjects(prefix: String): List<String> =
            s3Client.listObjects(bucketName, prefix)
                    .objectSummaries
                    .map { it.key }

    fun resourceUrl(key: String): URL =
            s3Client.getUrl(bucketName, key)

    suspend fun downloadObject(from: String, to: String): Boolean {
        try {
            return withContext(Dispatchers.IO) {
                s3Client.getObject(
                        GetObjectRequest(bucketName, from),
                        File(to)
                )
                return@withContext true
            }
        }catch (ase: AmazonServiceException){
            logger.error("service was not able to process file", ase.errorMessage, ase.statusCode, ase.errorCode, ase.errorType, ase.requestId)
            return false

        }catch (ace: SdkClientException){
            logger.error("service not responding ", ace.message)
            return false
        }
    }

    suspend fun getFilePath(serial: String): File = withContext(Dispatchers.IO) {
        if(!File(tempPath).exists())
            File(tempPath).mkdir()
        File(tempPath, "$serial-original")
    }
}

enum class ContentType(val typeName: String) {
    ZIP_FILE("application/zip"),
    PDF_FILE("application/pdf"),
    EPUB_FILE("application/epub+zip")
}

data class PutObjectRequest(
        val key: String,
        val input: InputStream,
        val length: Long,
        val type: ContentType,
        val userMeta: Map<String, String> = emptyMap()
)