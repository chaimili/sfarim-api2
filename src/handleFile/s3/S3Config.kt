package com.lomdaat.handleFile.s3

import io.micronaut.context.annotation.ConfigurationProperties

@ConfigurationProperties("s3")
interface S3Config {
    val endpoint: String
    val bucket: String
    val accessKey: String
    val secretKey: String
}