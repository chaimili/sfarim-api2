package odooResponses

import util.OdooFalseDate
import util.OdooFalseDouble
import util.OdooFalseInt
import util.OdooFalseString
import java.util.*

/**
 * Returned class of license details in odoo
 */
data class OdooLicense(
        val id: Int,
        @OdooFalseDate
        val expires_at: String,
        @OdooFalseString
        val status: String
)

/**
 * Return class of license kind details in odoo
 */
data class OdooLicenseKind(
        val id: Int,
        @OdooFalseString
        val name: String,
        @OdooFalseInt
        val days: Int,
        @OdooFalseDouble
        val precentage: Double,
        @OdooFalseDate
        val write_date: String
)

data class OdooLicenseKinds(
        val odooLicenseKind: List<OdooLicenseKind>,
        val last_updated: Date
)