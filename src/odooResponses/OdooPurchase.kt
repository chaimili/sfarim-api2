package odooResponses

import util.OdooFalseDouble
import util.OdooFalseString
import com.squareup.moshi.Json

data class OdooPurchase(
        val id: Int,
        @OdooFalseString
        val name: String,
        @OdooFalseDouble
        val list_price: Double
)

sealed class PaymentResponse(val statusCode: Int, val errorMessage: String? = null) {

        data class Success(
                @Json(name = "need_approval")
                val needsSupervisorApproval: Boolean
        ) : PaymentResponse(200)
        object CustomerNotFound : PaymentResponse(110, "Customer not found") // TODO: Check the status code?

        data class PaymentNotAccepted(val message: String): PaymentResponse(402, message)
        object PaymentSuccededWithFailedRegistration: PaymentResponse(403, "Payment succeeded with failed registration.")
        object SellerIdNotValid : PaymentResponse(420, "Seller id not valid.")
        object UnknownError: PaymentResponse(500, "Unknown error")
}



//inline fun <reified T: PaymentResponse> T.toJson() = ServerResponse.Json(this)
//
//fun PaymentResponse.toError() = ServerResponse.Error(statusCode, errorMessage ?: "")
//
//
//
//
//sealed class ServerResponse {
//        object Empty : ServerResponse()
//
//        data class Error(val statusCode: Int, val message: String, val exception: Exception? = null) : ServerResponse()
//
//        data class Json(val jsonObj: Any) : ServerResponse() {
//                val jsonString: String
//                        get() = Klaxon().toJsonString(jsonObj)
//        }
//}