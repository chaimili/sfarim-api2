package com.lomdaat

import util.OdooFalseInt
import util.OdooFalseString
import util.OdooIdsParser

/**
 * Returned class of userAccount details in odoo
 */
data class OdooUserAccount(
        val name: String,
        val id: Int,
        @OdooFalseString
        val zehut: String,
        @OdooFalseString
        val contact_address: String,
        @OdooFalseString
        val city: String,
        @OdooIdsParser
        val country_id: Int,
        @OdooFalseString
        val street: String,
        @OdooFalseInt
        val zip: Int = 0,
        @OdooFalseString
        var phone: String,
        @OdooFalseString
        val phone2: String,
        @OdooFalseString
        val mobile: String,
        @OdooFalseString
        val mobile2: String,
        @OdooFalseString
        val email: String,
        @OdooFalseString
        val my_create_on: String,
//        val is_lead: Boolean,
        @OdooFalseString
        val password: String
)

data class OdooUserAccountWithCreditCard(
        val name: String,
        val id: Int,
        val zehut: String,
        val contact_address: String,
        val city: String,
        val country_id: Int,
        val street: String,
        val zip: Int = 0,
        var phone: String,
        val phone2: String,
        val mobile: String,
        val mobile2: String,
        val email: String,
        val my_create_on: String,
        val password: String,
        var cc_id: String
)

data class UserAndPassword(
        val userAccount: OdooUserAccount?,
        val is_correct_password: Boolean
)

data class UserDetails(
        val userAccount: OdooUserAccountWithCreditCard?,
        val isRegistered: Boolean
)

data class OdooClient(
        @OdooIdsParser
        val customer_id: Int
)

data class OdooLastDigits(
        @OdooFalseString
        val name: String
)