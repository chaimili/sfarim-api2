package com.lomdaat

import util.OdooFalseInt
import util.OdooFalseString
import util.OdooIdsParser

/**
 * Returned class of coupon details in odoo
 */
data class OdooCouponInfo(
        val id: Int,
        @OdooFalseString
        val name: String,
        @OdooFalseString
        val description: String, // content
        @OdooIdsParser
        val book_id: Int,
        @OdooIdsParser
        val book_bundle_id: Int,
        @OdooFalseInt
        val days: Int
)

data class CouponInfoWithStatus(
        val status: Int,
        val couponInfo: OdooCouponInfo?
)

/**
 * Returned class of couponUnit details in odoo
 */
data class OdooCouponUnit(
        val id: Int,
        @OdooIdsParser
        val coupon_id: Int,
        val coupon_is_active: Boolean
)