package model

import com.lomdaat.CouponInfoWithStatus

interface CouponRepository {
    suspend fun checkBookCoupon(uuid: String, id: Int, password: String): Int
    suspend fun getCouponInfo(couponId: String): CouponInfoWithStatus
}