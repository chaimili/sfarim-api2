package model

import com.lomdaat.odooResponses.PreviewPages

interface UploadController {
    suspend fun handleUpload(serial: String)
    suspend fun getPreviewPages(serial: String): PreviewPages?
    suspend fun handleQueueUpload()
    suspend fun handleEpubUpload(serial: String)
}