package com.lomdaat.model

import com.lomdaat.*
import com.lomdaat.odooResponses.*
import model.data.Customer

interface SfarimRepository {
    suspend fun getCustomerByZehut(zehut: String): OdooUserAccount?
    suspend fun getCustomerById(id: Int): OdooUserAccount?
    suspend fun checkCustomerAndPassword(zehut: String, password: String): UserAndPassword
    suspend fun updateCustomerDetails(id: Int, customer: Customer): OdooUserAccount?
    suspend fun createCustomer(customer: Customer): OdooUserAccount?
    suspend fun checkCustomerAndPasswordById(id: Int, password: String): UserAndPassword
    suspend fun getCustomerByDeviceId(macAddress: String): UserDetails?
    suspend fun forgetPassword(id: Int, emailAddress: String?): Int


    suspend fun getBookList(customerId: Int, update_at: Long): List<OdooDigitalContentLicenseDetails>
    suspend fun getBundles(customerId: Int, update_at: Long): List<OdooBundleLicenseDetails>
    suspend fun getLastUpdatedBook(): OdooDigitalContent?
    suspend fun getLastUpdatedBundle(): OdooBundle?
    suspend fun getUnRenewableMagazine(customerId: Int, bundle_id: Int): Int
    suspend fun updateRateOfBook(serial_book: Long, rate: Int): Int

    suspend fun getLastDigits(customerId: Int, lastDigits: String): OdooCreditCard?
}
