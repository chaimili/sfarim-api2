package model

import odooResponses.OdooLicenseKinds

interface LicenseRepository {
    suspend fun getLicenseKinds(updated_at: Long): OdooLicenseKinds
}