package odooResponses.repositories.impl

import com.lomdaat.convertToDate
import com.lomdaat.convertUnixTimeToDate
import com.lomdaat.util.JsonRpc.JsonRpcOdooClient
import com.lomdaat.util.JsonRpc.searchAndRead
import model.LicenseRepository
import odooResponses.OdooLicenseKind
import odooResponses.OdooLicenseKinds

class OdooLicenseRepositoryImpl(val odooClient: JsonRpcOdooClient) : LicenseRepository {

    /**
     * Return list of LicenseKinds
     */
    override suspend fun getLicenseKinds(updated_at: Long): OdooLicenseKinds =
            OdooLicenseKinds(odooClient.searchAndRead<OdooLicenseKind>("license.kind", listOf(),
                    listOf("id", "name", "days", "precentage", "write_date"))
                    .filter { convertToDate(it.write_date).after(convertUnixTimeToDate(updated_at)) }.toList(),
                    convertToDate(getLastLicenseUpdated().write_date))

    //return lastUpdated license
    private suspend fun getLastLicenseUpdated() =
            odooClient.searchAndRead<OdooLicenseKind>("license.kind", listOf(),
                    listOf("id", "name", "days", "precentage", "write_date"))
                    .toSortedSet(compareByDescending { it.write_date }).first()

}