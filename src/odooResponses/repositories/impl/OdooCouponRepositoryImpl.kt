package odooResponses.repositories.impl

import com.lomdaat.*
import com.lomdaat.model.SfarimRepository
import com.lomdaat.util.JsonRpc.JsonRpcOdooClient
import com.lomdaat.util.JsonRpc.eq
import com.lomdaat.util.JsonRpc.searchAndRead
import controllers.COUPON_IN_USED
import controllers.INVALID_CUSTOMER_ID
import controllers.INVALID_PASSWORD
import controllers.INVALID_UUID
import model.CouponRepository
import odooResponses.OdooLicense
import org.kodein.di.generic.instance
import java.time.Instant
import java.util.*

class OdooCouponRepositoryImpl(val odooClient: JsonRpcOdooClient) : CouponRepository {

    private val sfarimRepository: SfarimRepository by kodein.instance<SfarimRepository>()

    /**
     * Make coupon used and make license on coupon with customer
     */
    override suspend fun checkBookCoupon(uuid: String, id: Int, password: String): Int {
        var expires_at: Long = Date.from(Instant.now()).time
        var status = "purchased"
        var digitalType = "book_id"
        var digitalTypeValue = 0
        val customer = sfarimRepository.checkCustomerAndPasswordById(id, password)
        if (customer.userAccount != null && customer.is_correct_password) {
            val couponUnit = getOdooCouponUnit(uuid)
            if (couponUnit != null) {
                if (couponUnit.coupon_is_active) {
                    useCouponUnit(couponUnit)
                    val coupon = getOdooCoupon(couponUnit.coupon_id)
                    if (coupon!!.days > 0) {
                        expires_at += coupon.days * 60 * 60 * 24 * 1000
                        status = "rented"
                    }
                    if (coupon.book_bundle_id > 0) {
                        digitalType = "bundle_id"
                        digitalTypeValue = coupon.book_bundle_id
                    } else
                        digitalTypeValue = coupon.book_id

                    val license = odooClient.searchAndRead<OdooLicense>("lomdaat.license", listOf(
                            "customer_id" eq customer.userAccount.id,
                            digitalType eq digitalTypeValue
                    ), listOf("id", "expires_at", "status")).firstOrNull()
                    if (license != null) {
                        if (convertToDate(license.expires_at).after(Date.from(Instant.now())) || license.status == "purchased")
                            return COUPON_IN_USED
                        //update license
                        if (odooClient.write("lomdaat.license", license.id, mapOf(
                                        "write_date" to Date.from(Instant.now()).time,
                                        "expires_at" to convertDateToString(expires_at),
                                        "status" to status
                                ))) return 1

                    } else
                        return odooClient.create("lomdaat.license", mapOf(
                                "expires_at" to convertDateToString(expires_at),
                                "status" to status,
                                "customer_id" to customer.userAccount.id,
                                digitalType to digitalTypeValue))
                }
                return COUPON_IN_USED
            }
            return INVALID_UUID
        } else
            if (customer.userAccount == null)
                return INVALID_CUSTOMER_ID
        return INVALID_PASSWORD
    }

    /**
     * Return coupon details find by uuid
     */
    override suspend fun getCouponInfo(couponId: String): CouponInfoWithStatus {
        val couponUnit = getOdooCouponUnit(couponId)
        if (couponUnit != null) {
            if (couponUnit.coupon_is_active)
                return CouponInfoWithStatus(1, getOdooCoupon(couponUnit.coupon_id))
            return CouponInfoWithStatus(2, getOdooCoupon(couponUnit.coupon_id))
        }
        return CouponInfoWithStatus(3, null)
    }

    // make coupon active - false
    private suspend fun useCouponUnit(couponUnit: OdooCouponUnit) =
            odooClient.write("coupon.units", couponUnit.id, mapOf("coupon_is_active" to false))

    // return CouponUnit
    private suspend fun getOdooCouponUnit(couponId: String): OdooCouponUnit? =
            odooClient.searchAndRead<OdooCouponUnit>("coupon.units", listOf(
                    "uuid" eq couponId
            ), listOf("id", "coupon_id", "coupon_is_active")).firstOrNull()

    // return couponInfo
    private suspend fun getOdooCoupon(coupon_id: Int): OdooCouponInfo? =
            odooClient.searchAndRead<OdooCouponInfo>("books.coupons", listOf(
                    "id" eq coupon_id
            ), listOf("id", "name", "description", "book_id", "book_bundle_id", "days")).firstOrNull()

}