package odooResponses.repositories.impl

import com.lomdaat.handleFile.s3.ContentType
import com.lomdaat.handleFile.s3.PutObjectRequest
import com.lomdaat.handleFile.s3.S3ObjectStorage
import com.lomdaat.logger
import com.lomdaat.odooResponses.PreviewPages
import com.lomdaat.util.JsonRpc.JsonRpcOdooClient
import com.lomdaat.util.JsonRpc.callMethod
import com.lomdaat.util.JsonRpc.eq
import com.lomdaat.util.JsonRpc.searchAndRead
import handleFile.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import model.UploadController
import java.io.File
import java.io.FileInputStream

const val tempPath = "/tmp/workspace"

class OdooUploadController(
    private val odooClient: JsonRpcOdooClient, private val handleFile: HandleFile,
    private val queueKeyPrefix: String, private val previewKeyPrefix: String,
    private val bookKeyPrefix: String, private val s3ObjectStorage: S3ObjectStorage,
    private val encryption: BookEncryption
) : UploadController {

    override suspend fun handleUpload(serial: String) = withContext(Dispatchers.IO) {

        logger.debug { "handleUpload started" }

        try {
            val file = s3ObjectStorage.getFilePath(serial)
            logger.debug { "handleUpload file:$file" }

            if (s3ObjectStorage.downloadObject("$queueKeyPrefix$serial", file.absolutePath)) {

                logger.debug { "handleUpload file downloaded" }

                val pageRange = getPreviewPages(serial)

                logger.debug { "handleUpload range:$pageRange" }

                val previewFile = handleFile.splitToPreview(file, serial, pageRange)

                logger.debug { "handleUpload preview:$previewFile" }

                val fileSplit = handleFile.splitFile(file, serial){ f, i ->
                    encryption.encrypt(f, "$serial-${i}", serial)
                }

                logger.debug { "handleUpload spilt:$fileSplit" }

                val zipFile = handleFile.zipFolder(fileSplit.first.absolutePath, fileSplit.first.absolutePath + ".zip")

                logger.debug { "handleUpload zip:$zipFile" }

                FileInputStream(previewFile).use {
                    s3ObjectStorage.putObject(
                        PutObjectRequest(
                            "$previewKeyPrefix$serial.pdf", it, previewFile.length(), ContentType.PDF_FILE
                        ), true
                    )
                }

                val previewUrl: String = s3ObjectStorage.resourceUrl("$previewKeyPrefix$serial.pdf").toString()

                logger.debug { "handleUpload preview:$previewUrl" }

                logger.info { "zip file size: ${zipFile.length()}" }

                FileInputStream(zipFile).use {
                    s3ObjectStorage.putObject(
                        PutObjectRequest(
                            "$bookKeyPrefix$serial", it,
                            zipFile.length(), ContentType.ZIP_FILE
                        ), false
                    )
                }

                s3ObjectStorage.deleteObject("$queueKeyPrefix$serial")

                odooClient.callMethod<String>(
                    "books.lomdaat", "upload_finish",
                    listOf(
                        mapOf(
                            "serial" to serial,
                            "preview_url" to previewUrl,
                            "file_size" to fileSplit.first.length(),
                            "page_count" to fileSplit.second
                        )
                    )
                )
            }
        } finally {
            File(tempPath).deleteRecursively()
        }
    }

    override suspend fun getPreviewPages(serial: String): PreviewPages =
        odooClient.searchAndRead<PreviewPages>(
            "books.lomdaat",
            listOf("product_number" eq serial),
            listOf("start_preview_page", "end_preview_page")
        ).firstOrNull().let {
            PreviewPages(
                startPage = if(it?.startPage != 0) it?.startPage ?: 1 else 1 ,
                endPage = if(it?.endPage != 0) it?.endPage ?: 4 else  4
            )
        }

    override suspend fun handleQueueUpload() {
        s3ObjectStorage.listObjects(queueKeyPrefix).forEach {
            handleUpload(it)
        }
    }

    override suspend fun handleEpubUpload(serial: String) = withContext(Dispatchers.IO) {
        val file = s3ObjectStorage.getFilePath(serial)
        try {
            if (encryption.encrypt(file, serial, "")) {
                val encryptedFile = File(tempPath, serial)
                FileInputStream(encryptedFile).use {
                    s3ObjectStorage.putObject(
                        PutObjectRequest(
                            "$bookKeyPrefix$serial", it,
                            encryptedFile.length(), ContentType.EPUB_FILE
                        ), false
                    )
                }

                s3ObjectStorage.deleteObject("$queueKeyPrefix$serial")

                odooClient.callMethod<String>(
                    "books.lomdaat", "epub_upload_finish",
                    listOf(
                        mapOf(
                            "serial" to serial,
                            "file_size" to encryptedFile.length()
                        )
                    )
                )
            }
        } finally {
            File(tempPath).deleteRecursively()
        }
    }
}
