package odooResponses.repositories.impl

import com.lomdaat.*
import com.lomdaat.model.SfarimRepository
import com.lomdaat.odooResponses.*
import com.lomdaat.util.JsonRpc.*
import controllers.CANT_SAVE_RATE_TO_BOOK
import controllers.DIGITAL_CONTENT_NOT_FOUND
import controllers.INVALID_BUNDLE_ID
import controllers.INVALID_CUSTOMER_ID
import model.data.CreditCard
import model.data.Customer
import odooResponses.OdooLicense
import handleEmail.SendEmail
import java.util.*
import java.time.Instant

class OdooSfarimRepositoryImpl(val odooClient: JsonRpcOdooClient) : SfarimRepository {

    override suspend fun getCustomerByZehut(zehut: String): OdooUserAccount? {
        return odooClient.searchAndRead<OdooUserAccount>("res.partner", listOf(
                "|",
                "zehut" eq zehut,
                "email" eq zehut
        ), listOfCustomerDetails).firstOrNull()
    }

    override suspend fun getCustomerById(id: Int): OdooUserAccount? =
            odooClient.searchAndRead<OdooUserAccount>("res.partner", listOf(
                    "id" eq id
            ), listOfCustomerDetails).firstOrNull()

    override suspend fun checkCustomerAndPassword(zehut: String, password: String): UserAndPassword {
        val customer = getCustomerByZehut(zehut)
        if (customer != null) {
            val result = odooClient.search("res.partner", listOf(
                    ("password" eq password),
                    "|", ("zehut" eq zehut),
                    ("email" eq zehut)
            )).firstOrNull()
            if (result != null)
                return UserAndPassword(customer, true)
            else
                return UserAndPassword(customer, false)
        } else
            return UserAndPassword(null, false)
    }

    override suspend fun createCustomer(customer: Customer): OdooUserAccount? {
        val isAppear = customerIsExist(customer)
        if (isAppear.isEmpty()) {
            odooClient.create("res.partner", customer.toMap())
            val userAccount = getCustomerByZehut(customer.zehut)
            if (customer.creditCard.cc_id != "")
                createCreditCard(customer.creditCard, userAccount!!.id)
            return userAccount
        }
        return null
    }

    override suspend fun updateCustomerDetails(id: Int, customer: Customer): OdooUserAccount? {
        odooClient.write("res.partner", id, customer.toMap())
        return getCustomerById(id)
    }

    /**
     * Return the last updated book
     */
    override suspend fun getLastUpdatedBook(): OdooDigitalContent? =
            odooClient.searchAndRead<OdooDigitalContent>("books.lomdaat", listOf(),
                    listOfDigitalContentDetails)
                    .toSortedSet(compareByDescending { it.write_date }).firstOrNull()

    /**
     * Return the last updated bundle
     */
    override suspend fun getLastUpdatedBundle(): OdooBundle? =
            odooClient.searchAndRead<OdooBundle>("books.bundles", listOf(),
                    listOfBundleDetails)
                    .toSortedSet(compareByDescending { it.write_date }).firstOrNull()

    /**
     * Return list of books with license details and tags, that updated after update_at
     */
    override suspend fun getBookList(customerId: Int, update_at: Long): List<OdooDigitalContentLicenseDetails> {
        val listBooks = odooClient.searchAndRead<OdooDigitalContent>("books.lomdaat",
                listOf(), listOfDigitalContentDetails)
                .filter {
                    filterToUpdateAfter(customerId, it.write_date, update_at, it.id, "book")
                }

        var listBooksWithTags = emptyList<OdooDigitalContentLicenseDetails>()
        listBooks.map {
            val license = if (customerId > 0) getOdooLicenseWithBook(it.id, customerId) else null
            listBooksWithTags += OdooDigitalContentLicenseDetails(it, getBookTag(it.id), license?.expires_at
                    ?: "", license?.status ?: "")
        }
        return listBooksWithTags
    }

    /**
     * Return list of bundles with license details, that updated after update_at
     */
    override suspend fun getBundles(customerId: Int, update_at: Long): List<OdooBundleLicenseDetails> {
        val basicBundles = odooClient.searchAndRead<OdooBundle>("books.bundles", listOf(),
                listOfBundleDetails)
                .filter {
                    filterToUpdateAfter(customerId, it.write_date, update_at, it.id, "bundle")
                }
        var listOfBundles = emptyList<OdooBundleLicenseDetails>()
        basicBundles.map {
            val license = if (customerId > 0) getOdooLicenseWithBundle(it.id, customerId) else null
            listOfBundles += OdooBundleLicenseDetails(it, license?.expires_at ?: "", license?.status ?: "")
        }
        return listOfBundles
    }

    override suspend fun getUnRenewableMagazine(customerId: Int, bundle_id: Int): Int {
        if (getCustomerById(customerId) != null) {
            if (odooClient.search("books.bundles", listOf(
                            "id" eq bundle_id
                    )).count() > 0) {
                val id = getOdooLicenseWithBundle(bundle_id, customerId)

                if (odooClient.write("lomdaat.license", id?.id?.toInt() ?: return 0, mapOf(
                                "status" to "is_ending",
                                "write_date" to Date.from(Instant.now()).time
                        ))) return 1
                else return 0
            }
            return INVALID_BUNDLE_ID
        }
        return INVALID_CUSTOMER_ID
    }

    /**
     * Add rate of book to rate_sum in book and count the raters
     */
    override suspend fun updateRateOfBook(serial_book: Long, rate: Int): Int {
        val result = odooClient.searchAndRead<OdooDigitalContent>("books.lomdaat", listOf(
                "product_number" eq serial_book
        ), listOfDigitalContentDetails).firstOrNull()
        if (result != null) {
            if (odooClient.write("books.lomdaat", result.id, mapOf(
                            "rate_sum" to result.rate_sum + rate,
                            "raters" to result.raters + 1
                    )))
                return result.rate_sum + rate
            return CANT_SAVE_RATE_TO_BOOK
        }
        return DIGITAL_CONTENT_NOT_FOUND
    }

    override suspend fun getLastDigits(customerId: Int, lastDigits: String): OdooCreditCard? {
        val id = odooClient.callMethod<Int>("res.partner", "get_default_credit_card_id",
                listOf(mapOf("customer_id" to customerId)))

        return odooClient.searchAndRead<OdooCreditCard>("lomdaat.credit_card", listOf(
                "id" eq id
        ), listOf("name", "text_expiration", "cc_id")).firstOrNull()
    }

    /**
     * Validate customer with password
     */
    override suspend fun checkCustomerAndPasswordById(id: Int, password: String): UserAndPassword {
        val customer = getCustomerById(id)
        if (customer != null) {
            val result = odooClient.search("res.partner", listOf(
                    ("password" eq password),
                    ("id" eq id)
            )).firstOrNull()
            if (result != null)
                return UserAndPassword(customer, true)
            else
                return UserAndPassword(customer, false)
        } else
            return UserAndPassword(null, true)
    }

    /**
     * Change Password and send mail to user
     */
    override suspend fun forgetPassword(id: Int, emailAddress: String?): Int {
        val newPassword = (1000..9999).shuffled().last()
        changePassword(id, newPassword.toString())
        if (emailAddress != null)
            SendEmail().sendMessage(newPassword, emailAddress)
        return newPassword
    }

    /**
     * Find user by deviceId
     */
    override suspend fun getCustomerByDeviceId(macAddress: String): UserDetails? {
        val response = odooClient.searchAndRead<OdooClient>("lomdaat_mdm.device", listOf(
                "device_no" eq macAddress)
                , listOf("customer_id")).firstOrNull()

        if(response != null)
            return if(response.customer_id > 0){
                val id = odooClient.callMethod<Int>("res.partner", "get_default_credit_card_id",
                        listOf(mapOf("customer_id" to response.customer_id)))
                val lastDigits = odooClient.searchAndRead<OdooLastDigits>("lomdaat.credit_card", listOf(
                        "id" eq id
                ), listOf("name")).firstOrNull()

                val customer = getCustomerById(response.customer_id)
                UserDetails(getUserWithCC(customer!!, lastDigits), true)
            }
            else
                UserDetails(null, false)
        return null
    }

    // return tag to book
    private suspend fun getBookTag(id: Int) =
            odooClient.searchAndRead<OdooBookTag>("book.tags.books", listOf(
                    "book_id" eq id
            ), listOf("book_tag_id"))

    // return license to bundle and customer
    private suspend fun getOdooLicenseWithBundle(bundle_id: Int, customerId: Int): OdooLicense? =
            odooClient.searchAndRead<OdooLicense>("lomdaat.license", listOf(
                    "bundle_id" eq bundle_id,
                    "customer_id" eq customerId
            ), listOf("id", "expires_at", "status")).firstOrNull()

    // return license to book and customer
    private suspend fun getOdooLicenseWithBook(book_id: Int, customerId: Int): OdooLicense? =
            odooClient.searchAndRead<OdooLicense>("lomdaat.license", listOf(
                    "book_id" eq book_id,
                    "customer_id" eq customerId
            ), listOf("id", "expires_at", "status")).firstOrNull()

    // check if customer is already exist
    private suspend fun customerIsExist(customer: Customer): List<Int> =
            odooClient.search("res.partner", listOf(
                    "zehut" eq customer.zehut
            ))

    private suspend fun createCreditCard(creditCard: CreditCard, id: Int) =
            odooClient.create("lomdaat.credit_card", creditCard.toMap()
                    + ("customer_id" to id))

    val listOfDigitalContentDetails = listOf("author", "copyright", "country_id", "currency_id", "discount",
            "edition", "format_id", "id", "raters", "rate_sum", "language_id", "availability", "name", "pages_count",
            "digital_type", "price", "publication_date", "summary", "genere_id", "can_be_rented", "can_be_posted",
            "subscribe", "post_price", "priority", "create_date", "write_date", "product_number")

    val listOfCustomerDetails = listOf("name", "id", "zehut", "contact_address", "city", "country_id",
            "street", "zip", "phone", "phone2", "mobile", "mobile2", "email", "my_create_on", "password")

    val listOfBundleDetails = listOf("post_price", "can_be_posted", "can_be_rented", "is_magazine",
            "distribution_in", "id", "name", "price", "rent_month_price", "books", "write_date")

    private fun CreditCard.toMap() = mapOf(
            "text_expiration" to text_expiration,
            "cc_id" to cc_id,
            "cvv" to cvv,
            "state" to "draft",
            "token" to false
    )

    // make map of customer details
    private fun Customer.toMap() = mapOf(
            "zehut" to zehut,
            "name" to "$firstname $lastname",
            "street" to street,
            "city" to city,
            "phone" to cli,
            "password" to password,
            "email" to email,
            "mobile" to mobile_number1
    ).filter { it.value.isNotEmpty() && it.value.isNotBlank() }

    // check if write_date of digitalType or bundle or license on them, is after update_at that get from user
    private suspend fun filterToUpdateAfter(customerId: Int, write_date: String, update_at: Long, id: Int, type: String): Boolean {
        var date =
                if (type == "book")
                    getOdooLicenseWithBook(id, customerId)?.expires_at ?: "2000-01-01 00:00:00"
                else
                    getOdooLicenseWithBundle(id, customerId)?.expires_at ?: "2000-01-01 00:00:00"
        if (date.isEmpty())
            date = "2000-01-01 00:00:00"
        return if (customerId > 0) {
                convertToDate(write_date).after(convertUnixTimeToDate(update_at)) or
                        convertToDate(date).after(convertUnixTimeToDate(update_at))
        } else
            convertToDate(write_date).after(convertUnixTimeToDate(update_at))
    }

    // change password to user in the server
    private suspend fun changePassword(id: Int, password: String) =
            odooClient.write("res.partner", id, mapOf(
                    "password" to password
            ))

    private fun getUserWithCC(odooUserAccount: OdooUserAccount, creditCard: OdooLastDigits?) =
            OdooUserAccountWithCreditCard(
                    odooUserAccount.name,
                    odooUserAccount.id,
                    odooUserAccount.zehut,
                    odooUserAccount.contact_address,
                    odooUserAccount.city,
                    odooUserAccount.country_id,
                    odooUserAccount.street,
                    odooUserAccount.zip,
                    odooUserAccount.phone,
                    odooUserAccount.phone2,
                    odooUserAccount.mobile,
                    odooUserAccount.mobile2,
                    odooUserAccount.email,
                    odooUserAccount.my_create_on,
                    odooUserAccount.password,
                    creditCard?.name ?: ""
            )
}