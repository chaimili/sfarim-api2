package odooResponses

import io.micronaut.context.annotation.ConfigurationProperties

@ConfigurationProperties("odoo")
interface OdooConfig {
    val user: String
    val pass: String
    val host: String
    val port: String
    val db: String
}