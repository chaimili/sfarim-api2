package com.lomdaat.odooResponses

import com.squareup.moshi.Json
import util.*

/**
 * Returned class of digitalContent details in odoo
 */
data class OdooDigitalContent(
        @OdooFalseString
        val author: String,
        @OdooFalseString
        val copyright: String?,
        @OdooIdsSecondParser
        val country_id: String,
        @OdooFalseDate
        val write_date: String,
        @OdooFalseDate
        val create_date: String,
        @OdooIdsSecondParser
        val currency_id: String,
        @OdooFalseInt
        val discount: Int,
        @OdooFalseInt
        val edition: Int,
        @OdooIdsSecondParser
        val format_id: String,
        @OdooFalseInt
        val id: Int,
        @OdooFalseInt
        val raters: Int,
        @OdooFalseInt
        val rate_sum: Int,
        @OdooIdsSecondParser
        val language_id: String,
        @OdooIdsSecondParser
        val availability: String,
        @OdooFalseString
        val name: String,
        @OdooFalseInt
        val pages_count: Int,
        @OdooIdsSecondParser
        val digital_type: String,
//        @OdooFalseString
//        val link: String,
//        val book_link: String = "",
//        val cover_link: String = "",
        @OdooFalseDouble
        val price: Double,
        @OdooFalseDate
        val publication_date: String,
        @OdooFalseString
        val summary: String,
        @OdooIdsSecondParser
        val genere_id: String,
        val can_be_rented: Boolean,
        val can_be_posted: Boolean,
        val subscribe: Boolean,
        @OdooFalseDouble
        val post_price: Double,
        @OdooFalseInt
        val priority: Int,
        val product_number: Long
)

/**
 * Class of digitalContent details and license details on it
 */
data class OdooDigitalContentLicenseDetails(
        val odooDigitalContent: OdooDigitalContent,
        val tags: List<OdooBookTag>,
        val expires_at: String,
        val status: String
)

/**
 * Returned name of tag to book
 */
data class OdooBookTag(
        @OdooIdsSecondParser
        val book_tag_id: String
)

data class OdooCreditCard(
        @OdooFalseString
        val name: String,
        @OdooFalseString
        val text_expiration: String,
        @OdooFalseString
        val cc_id: String
)

/**
 * Returned class of bundle details in odoo
 */
data class OdooBundle(
        @OdooFalseDouble
        val post_price: Double,
        val can_be_posted: Boolean,
        val can_be_rented: Boolean,
        val is_magazine: Boolean,
        @OdooFalseString
        val distribution_in: String,
        val id: Int,
        @OdooFalseString
        val name: String,
        @OdooFalseDouble
        val price: Double,
        @OdooFalseDouble
        val rent_month_price: Double,
        @OdooObjectToList
        val books: List<*>,
        @OdooFalseDate
        val write_date: String
)

/**
 * Class of bundle details and license details on it
 */
data class OdooBundleLicenseDetails(
        val odooBundle: OdooBundle,
        val expires_at: String,
        val status: String
)

data class PreviewPages(
        @Json(name = "start_preview_page")
        val startPage: Int,
        @Json(name = "end_preview_page")
        val endPage: Int
)