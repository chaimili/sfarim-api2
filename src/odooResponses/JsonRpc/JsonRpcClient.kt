package com.lomdaat.util.JsonRpc

import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import io.ktor.client.HttpClient
import io.ktor.client.call.TypeInfo
import io.ktor.client.features.json.JsonSerializer
import io.ktor.client.request.get
import io.ktor.client.response.HttpResponse
import io.ktor.client.response.readText
import io.ktor.content.TextContent
import io.ktor.http.ContentType
import io.ktor.http.content.OutgoingContent
import io.ktor.http.contentType
import java.lang.reflect.Type

interface JsonRpcClient {
    suspend fun <R> invoke(method: String, params: Any, resultType: Type): R
}

suspend inline fun <reified R> JsonRpcClient.invoke(method: String, params: Any): R =
        invoke(method, params, R::class.java)

data class JsonRpcResponse<out R>(val id: Int, val result: R? = null, val error: JsonRpcError? = null, val jsonrpc:String = "2.0")

data class JsonRpcError(val code: Int, val message: String, val data: Any?)

data class JsonRpcRequest(val id: Int, val method: String, val params: Any, val jsonrpc: String = "2.0")

class JsonRpcException(val code: Int, message: String, val data: Any?) : Exception()


class MoshiSerializer(block: Moshi.Builder.() -> Moshi = { build() }) : JsonSerializer {

    private val backend: Moshi = Moshi.Builder().run { block() }

    override fun write(data: Any): OutgoingContent = TextContent(backend.adapter<Any>(Object::class.java).toJson(data), ContentType.Application.Json)

    override suspend fun read(info: TypeInfo, response: HttpResponse): Any {
        val text= response.readText()
        return backend.adapter<Any>(info.type.java).fromJson(text)!!
    }
}


class KtorJsonRpcClient(val client: HttpClient, val moshi: Moshi, val endpoint: String): JsonRpcClient {
    override suspend fun <R> invoke(method: String, params: Any, resultType: Type): R {
        val stringResponse = client.get<String>(endpoint) {
            contentType(ContentType.Application.Json)

            body = JsonRpcRequest(1, method, params)
        }

	    val responseType = Types.newParameterizedType(JsonRpcResponse::class.java, resultType)
	    val response = moshi.adapter<JsonRpcResponse<R>>(responseType).fromJson(stringResponse)!!

        if (response.result != null)
            return response.result
        else
            throw response.error!!.run { JsonRpcException(code, message, data) }
    }
}