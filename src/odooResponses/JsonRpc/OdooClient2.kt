package com.lomdaat.util.JsonRpc

import java.lang.reflect.Type

infix fun String.eq(value: Any) = listOf(this, "=", value)

interface OdooClient2 {
    fun login(user: String, pass: String)

    /** This is an example domain: listOf("|", listOf("is_company", "=", false), listOf("customer", "=", true)) */
    suspend fun search(model: String, searchDomain: List<Any>): List<Int>

    suspend fun <R> searchAndRead(model: String, searchDomain: List<Any>, fields: List<String>? = null, limit: Int? = null, type: Type): List<R>

    suspend fun <R> read(model: String, recIds: List<Int>, fields: List<String>, type: Type): List<R>

    suspend fun create(model: String, fieldsToUpdate: Map<String, Any>): Int

    suspend fun write(model: String, rowId: Int, fieldsToUpdate: Map<String, Any>): Boolean

    suspend fun <T> callMethod(model: String, params: List<Any>, returnType: Class<T>): T

    suspend fun <R> callMethod(
            model: String,
            methodName: String,
            params: List<Any> = emptyList(),
            paramMap: Map<String, Any> = emptyMap(),
            returnType: Type
    ): R
}

suspend inline fun <reified R> OdooClient2.searchAndRead(model: String, searchDomain: List<Any>, fields: List<String>? = null, limit: Int? = null): List<R> =
		searchAndRead(model, searchDomain, fields, limit, R::class.java)

suspend inline fun <reified R> OdooClient2.read(model: String, recIds: List<Int>, fields: List<String>): List<R> =
        read(model, recIds, fields, R::class.java)

suspend inline fun <reified R> OdooClient2.callMethod(model: String, methodName: String, params: List<Any> = emptyList(), paramMap: Map<String, Any> = emptyMap()): R =
        callMethod(model, methodName, params, paramMap, R::class.java)