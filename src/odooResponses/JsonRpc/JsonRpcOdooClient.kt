package com.lomdaat.util.JsonRpc

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.squareup.moshi.Types
import kotlinx.coroutines.runBlocking
import org.slf4j.LoggerFactory
import java.lang.reflect.Type

class JsonRpcOdooClient(
        private val rpcClient: JsonRpcClient,
        private val db: String
): OdooClient2 {
    var userId: Int = -1
    private var pass: String = ""

    override fun login(user: String, pass: String) {
        val d = mapOf("service" to "common", "method" to "login", "args" to listOf(db, user, pass))

        userId = runBlocking { rpcClient.invoke<Int>("call", d) }
        this.pass = pass
    }

    fun base(methodName: String, model: String, args: List<Any>?, map: Map<String, Any>? = emptyMap()) = mapOf(
            "service" to "object",
            "method" to "execute_kw",
            "args" to listOf(db, userId, pass, model, methodName, args, map)
    )

    override suspend fun search(model: String, searchDomain: List<Any>): List<Int> =
            rpcClient.invoke<List<Float>>("call", base("search", model, listOf(
                    searchDomain
            ))).map { it.toInt() }

    private fun kwargs(vararg args: Pair<String, Any?>): Map<String, Any> =
            args.filter { it.second != null }.toMap() as Map<String, Any>

    override suspend fun <R> searchAndRead(model: String, searchDomain: List<Any>, fields: List<String>?, limit: Int?, type: Type): List<R> {
        val resultType = Types.newParameterizedType(List::class.java, type)

        return rpcClient.invoke("call", base("search_read", model, listOf(
                searchDomain
        ), kwargs(
                "fields" to fields,
                "limit" to limit
        )), resultType)
    }

    override suspend fun <R> read(model: String, recIds: List<Int>, fields: List<String>, type: Type): List<R> {
        val resultType = Types.newParameterizedType(List::class.java, type)
        return rpcClient.invoke("call", base("read", model, listOf(
                recIds
        ), mapOf(
                "fields" to fields
        )), resultType)
    }

    override suspend fun create(model: String, fieldsToUpdate: Map<String, Any>): Int =
        rpcClient.invoke("call", base("create", model, listOf(
                fieldsToUpdate
        )))


    override suspend fun write(model: String, rowId: Int, fieldsToUpdate: Map<String, Any>): Boolean =
        rpcClient.invoke("call", base("write", model, listOf(
                rowId, fieldsToUpdate
        )))

    override suspend fun <T> callMethod(model: String, params: List<Any>, returnType: Class<T>): T {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override suspend fun <R> callMethod(model: String, methodName: String, params: List<Any>, paramMap: Map<String, Any>, returnType: Type): R {
//        val resultType = Types.newParameterizedType(List::class.java, returnType)
        return rpcClient.invoke("call", base(methodName, model, params, paramMap), returnType)
    }
}