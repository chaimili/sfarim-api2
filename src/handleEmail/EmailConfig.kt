package handleEmail

import io.micronaut.context.annotation.ConfigurationProperties

@ConfigurationProperties("email")
interface EmailConfig {
    val user: String
    val pass: String
    val host: String
    val port: Int
}