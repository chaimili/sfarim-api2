package handleEmail

import com.lomdaat.kodein
import org.kodein.di.generic.instance
import org.simplejavamail.email.EmailBuilder
import org.simplejavamail.mailer.MailerBuilder
import org.simplejavamail.mailer.config.TransportStrategy
import javax.mail.internet.InternetAddress

class SendEmail {

    private fun String.getConfig() = System.getenv(this)

    private val emailConfig: EmailConfig by kodein.instance<EmailConfig>()

     fun sendMessage(newPassword: Int, addressTo: String) {
        val email = EmailBuilder.startingBlank()
                .from(InternetAddress("support@hadran.net", "פניות הדרן-אין להשיב למייל זה"))
                .to(addressTo)
                .clearReplyTo()
                .withSubject("איפוס סיסמא")
                .withHTMLText("סיסמתך לשירותי הדרן אופסה ומעכשיו היא <b>$newPassword </b>")
                .buildEmail()

        MailerBuilder
                .withSMTPServer(
                        emailConfig.host,
                        emailConfig.port,
                        emailConfig.user,
                        emailConfig.pass
                )
                .withTransportStrategy(TransportStrategy.SMTP_TLS)
                .buildMailer()
                .sendMail(email)
    }
}