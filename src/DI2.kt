package com.lomdaat

import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.lomdaat.model.SfarimRepository
import odooResponses.repositories.impl.OdooSfarimRepositoryImpl
import com.lomdaat.util.JsonRpc.JsonRpcOdooClient
import com.lomdaat.util.JsonRpc.KtorJsonRpcClient
import com.lomdaat.util.JsonRpc.MoshiSerializer
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import io.ktor.client.HttpClient
import io.ktor.client.engine.apache.Apache
import org.kodein.di.Kodein
import java.net.ConnectException
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.client.builder.AwsClientBuilder
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.auth0.jwk.JwkProvider
import com.auth0.jwk.JwkProviderBuilder
import com.github.jasync.sql.db.Connection
import com.github.jasync.sql.db.postgresql.PostgreSQLConnectionBuilder
import com.lomdaat.auth.KeycloakConfig
import com.lomdaat.configs.ApiConfig
import com.lomdaat.configs.FolderUrlPrefixConfig
import com.lomdaat.handleFile.s3.S3Config
import com.lomdaat.licenseClient.LicenseClient
import handleFile.BookEncryption
import com.lomdaat.handleFile.s3.S3ObjectStorage
import com.lomdaat.postgresResponses.PostgresConfig
import handleEmail.EmailConfig
import handleFile.HandleFile
import java.util.concurrent.TimeUnit
import io.ktor.client.features.json.JsonFeature
import io.micronaut.context.ApplicationContext
import model.*
import postgresResponses.PostgresRepository
import postgresResponses.PostgresRepositoryImpl
import mu.KotlinLogging
import odooResponses.OdooConfig
import odooResponses.repositories.impl.OdooCouponRepositoryImpl
import odooResponses.repositories.impl.OdooLicenseRepositoryImpl
import odooResponses.repositories.impl.OdooUploadController
import org.kodein.di.generic.*
import postgresResponses.PostgresClient
import util.*
import util.BeginObjectAdapter
import util.DateAdapter
import util.DoubleAdapter
import util.IdAdapter
import util.IdSecondAdapter
import util.IntAdapter
import util.StringAdapter
import java.net.URL

fun String.getConfig(): String = kotlin.runCatching {
    System.getenv(this)!!
}.onFailure {
    logger.error { "Required environment variable '$this' is missing." }
}.getOrThrow()

val logger = KotlinLogging.logger { }


val kodein = Kodein.lazy {

    bind<Moshi>() with singleton {
        Moshi.Builder()
                .add(KotlinJsonAdapterFactory())
                .add(StringAdapter())
                .add(IntAdapter())
                .add(IdAdapter())
                .add(DoubleAdapter())
                .add(BeginObjectAdapter())
                .add(IdSecondAdapter())
                .add(DateAdapter())
                .build()
    }

    import(OdooClientModule)
    import(s3Module)
    import(postgresqlModule)
    import(keycloakConfigurationModule)

    bind<UploadController>() with singleton {
        val odooClient: JsonRpcOdooClient by kodein.instance<JsonRpcOdooClient>()
        val handleFile: HandleFile by kodein.instance<HandleFile>()
        val bookEncryption: BookEncryption by kodein.instance<BookEncryption>()
        val s3Prefixes = instance<S3Prefixes>()
        val s3ObjectStorage: S3ObjectStorage by kodein.instance<S3ObjectStorage>()
        OdooUploadController(odooClient, handleFile, s3Prefixes.queuePrefix,
                s3Prefixes.previewPrefix, s3Prefixes.bookPrefix, s3ObjectStorage, bookEncryption)
    }

    bind<SfarimRepository>() with singleton {
        val odooClient: JsonRpcOdooClient by kodein.instance<JsonRpcOdooClient>()
        OdooSfarimRepositoryImpl(odooClient)
    }

    bind<CouponRepository>() with singleton {
        val odooClient: JsonRpcOdooClient by kodein.instance<JsonRpcOdooClient>()
        OdooCouponRepositoryImpl(odooClient)
    }
    bind<LicenseRepository>() with singleton {
        val odooClient: JsonRpcOdooClient by kodein.instance<JsonRpcOdooClient>()
        OdooLicenseRepositoryImpl(odooClient)
    }

    bind<PostgresRepository>() with singleton {
        PostgresRepositoryImpl(instance())
    }

    bind<PostgresClient>() with singleton {
        PostgresClient(instance())
    }

    bind<ApplicationContext>("applicationContext") with singleton {
        ApplicationContext.run()
    }

    bind<LicenseClient>() with singleton {
        instance<ApplicationContext>("applicationContext").getBean(LicenseClient::class.java)
    }

    bind<BookEncryption>() with singleton {
        BookEncryption()
    }

    bind<HandleFile>() with singleton {
        HandleFile()
    }

    bind<ApiConfig>() with provider {
        instance<ApplicationContext>("applicationContext").getBean(ApiConfig::class.java)
    }

    bind<EmailConfig>() with provider {
        instance<ApplicationContext>("applicationContext").getBean(EmailConfig::class.java)
    }
}

val OdooClientModule = Kodein.Module("OdooClientModule") {

    bind<OdooConfig>() with provider {
        instance<ApplicationContext>("applicationContext").getBean(OdooConfig::class.java)
    }

    bind<String>("odooRpcEndpoint") with singleton {
        "http://${instance<OdooConfig>().host}:${instance<OdooConfig>().port}/jsonrpc"
    }

    bind() from singleton {
        HttpClient(Apache) {
            install(JsonFeature) {
                serializer = MoshiSerializer { instance() }
            }
        }
    }

    bind() from singleton { KtorJsonRpcClient(instance(), instance(), instance("odooRpcEndpoint")) }

    bind() from singleton { JsonRpcOdooClient(instance(), instance<OdooConfig>().db)}

    onReady {
        try {
            instance<JsonRpcOdooClient>().login(
                    instance<OdooConfig>().user, instance<OdooConfig>().pass
            )
        } catch (e: ConnectException) {
            logger.error("Didn't succeed in connecting to Odoo: ${instance<String>("odooRpcEndpoint")}", e)
        }

    }
}

val keycloakConfigurationModule = Kodein.Module("keycloakConfigurationModule") {

    bind<KeycloakConfig>() with provider {
        instance<ApplicationContext>("applicationContext").getBean(KeycloakConfig::class.java)
    }

    bind<String>("jwkRealm") with singleton {
        instance<KeycloakConfig>().realm
    }
    bind<String>("jwkIssuer") with singleton {
        "${instance<KeycloakConfig>().baseUrl}/auth/realms/${instance<KeycloakConfig>().realm}"
    }

    bind<JwkProvider>() with singleton {
        val jwkIssuer = instance<String>("jwkIssuer")
        JwkProviderBuilder(URL("$jwkIssuer/protocol/openid-connect/certs")).build()
    }
}

interface UrlPrefixes {
    val coverUrlPrefix: String
    val bundleUrlPrefix: String
    val previewUrlPrefix: String
}

data class S3Prefixes(
        val endpoint: String,
        val bucket: String,
        val coverPrefix: String,
        val bundlePrefix: String,
        val bookPrefix: String,
        val previewPrefix: String,
        val queuePrefix: String
) : UrlPrefixes {
    private fun String.toUrl() =
            "https://$endpoint/$bucket/$this"

    override val coverUrlPrefix = coverPrefix.toUrl()
    override val bundleUrlPrefix = bundlePrefix.toUrl()
    override val previewUrlPrefix = previewPrefix.toUrl()
    val queueUrlPrefix = queuePrefix.toUrl()
}

val s3Module = Kodein.Module("s3Module") {

    bind<S3Config>() with provider {
        instance<ApplicationContext>("applicationContext").getBean(S3Config::class.java)
    }

    bind<FolderUrlPrefixConfig>() with singleton {
        instance<ApplicationContext>("applicationContext").getBean(FolderUrlPrefixConfig::class.java)
    }

    bind<S3Prefixes>() with singleton {
        val s3Config = instance<S3Config>()
        val folderUrlPrefixConfig = instance<FolderUrlPrefixConfig>()
        S3Prefixes(
                s3Config.endpoint,
                s3Config.bucket,
                folderUrlPrefixConfig.cover,
                folderUrlPrefixConfig.bundle,
                folderUrlPrefixConfig.book,
                folderUrlPrefixConfig.preview,
                folderUrlPrefixConfig.queue
        )
    }

    bind<UrlPrefixes>() with provider {
        instance<S3Prefixes>()
    }

    bind<PresignedUrlGenerator>() with singleton {
        val folderUrlPrefixConfig = instance<FolderUrlPrefixConfig>()
        PresignedUrlGenerator(
                instance(),
                instance<S3Prefixes>().bucket,
                2,
                folderUrlPrefixConfig.book,
                folderUrlPrefixConfig.preview
        )
    }

    bind<AmazonS3>() with singleton {
        val s3Config = instance<S3Config>()
        val awsCredentials = BasicAWSCredentials(
                s3Config.accessKey,
                s3Config.secretKey
        )

        val endpointConfig = AwsClientBuilder.EndpointConfiguration(
                instance<S3Prefixes>().endpoint, null
        )

        AmazonS3ClientBuilder
                .standard()
                .withCredentials(AWSStaticCredentialsProvider(awsCredentials))
                .withEndpointConfiguration(endpointConfig)
                .build()

    }

    bind<S3ObjectStorage>() with singleton {
        S3ObjectStorage(
                instance(),
                instance<S3Prefixes>().bucket
        )
    }
}

val postgresqlModule = Kodein.Module("postgresqlModule") {

    bind<PostgresConfig>() with provider {
        instance<ApplicationContext>("applicationContext").getBean(PostgresConfig::class.java)
    }

    bind<Connection>() with singleton {
        val postgresConfig = instance<PostgresConfig>()
        PostgreSQLConnectionBuilder.createConnectionPool {
            host = postgresConfig.host
            port = postgresConfig.port.toInt()
            database = postgresConfig.db
            username = postgresConfig.user
            password = postgresConfig.pass

            maxIdleTime = TimeUnit.MINUTES.toMillis(15)
            maxPendingQueries = 10_000
            maxActiveConnections = 100
            connectionValidationInterval = TimeUnit.SECONDS.toMillis(30)
        }
    }

    onReady {
        instance<Connection>().connect().get(1L, TimeUnit.MINUTES)
    }
}