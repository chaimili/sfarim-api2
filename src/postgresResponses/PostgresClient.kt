package postgresResponses

import com.github.jasync.sql.db.Connection
import com.github.jasync.sql.db.ResultSet
import com.github.jasync.sql.db.RowData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.future.await
import kotlinx.coroutines.withContext
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException

class PostgresClient(val client: Connection) {

    suspend fun sendPreparedStatement(query: String, values: List<Any?>) : ResultSet? = withContext(Dispatchers.IO) {
        try {
            if(!client.isConnected())
                client.connect().get(1L, TimeUnit.MINUTES)
            client.sendPreparedStatement(query, values, true).await().rows
        } catch (e: TimeoutException){
            throw DBConnectivityException()
        }

    }
}

class DBConnectivityException : Exception()

inline fun <reified T> RowData.get(columnNumber: Int): T? =
    get(columnNumber) as? T