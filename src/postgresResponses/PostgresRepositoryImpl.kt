package postgresResponses

import com.github.jasync.sql.db.ResultSet
import com.github.jasync.sql.db.RowData
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import org.intellij.lang.annotations.Language
import java.math.BigDecimal

class PostgresRepositoryImpl(
        private val client: PostgresClient
) : PostgresRepository {

    /**
     * Return string of bookList from dataBase
     */
    override suspend fun getBooksList(update_at: Long, updated_licenses: List<BigDecimal?>): ResultSet? =
            client.sendPreparedStatement(SQL_bookList, listOf(update_at, updated_licenses))

    /**
     * Return string of bundlesList from dataBase
     */
    override suspend fun getBundlesList(update_at: Long, updated_licenses: List<BigDecimal?>): ResultSet? =
            client.sendPreparedStatement(SQL_bundleList, listOf(update_at, updated_licenses))

    companion object {
        @Language("PostgreSQL")
        val SQL_bookList = """
            SELECT b.id, b.name AS title, b.author, b.active, b.price, b.discount AS discount_percent,
                rp.name AS copyright, b.subscribe AS is_subscribe, b.summary, b.priority,
                EXTRACT(EPOCH FROM b.create_date:: timestamp):: bigint AS create_date,
                EXTRACT(EPOCH FROM b.write_date:: timestamp):: bigint AS write_date,
                b.can_be_rented, b.can_be_posted, b.post_price,
                COALESCE(to_char(b.publication_date, 'DD-MM-YYYY'), '') AS publication_date,
                b.edition, b.pages_count, b.rate_sum, b.raters, b.product_number,
                dt.name AS type, av.name AS availability, con.name AS country, cur.name AS currency,
                fr.name AS format, gen.name AS category, lan.name AS language, 
                (
                    SELECT array_to_json(array_agg(d))
                    FROM (
                        SELECT tag.name
                        FROM books_tags tag LEFT JOIN book_tags_books btag ON tag.id = btag.book_tag_id
                        WHERE btag.book_id = b.id
                    ) d
                ) AS book_tags
            FROM books_lomdaat b
            LEFT JOIN res_users ru
                ON b.copyright_id = ru.id
            LEFT JOIN res_partner rp
                ON rp.id = ru.partner_id
            LEFT JOIN books_kind dt 
                ON b.digital_type = dt.id
            LEFT JOIN books_availability av 
                ON b.availability = av.id
            LEFT JOIN books_country con 
                ON b.country_id = con.id
            LEFT JOIN books_coin cur 
                ON b.currency_id = cur.id
            LEFT JOIN books_formats fr 
                ON b.format_id = fr.id
            LEFT JOIN books_generes gen
                ON b.genere_id = gen.id
            LEFT JOIN books_language lan 
                ON b.language_id = lan.id
            WHERE b.write_date > to_timestamp(?) OR b.product_number = ANY(?)
            ORDER BY b.id ASC
        """.trimIndent()

        @Language("PostgreSQL")
        val SQL_bundleList = """
            SELECT bn.id, bn.name, bn.active, bn.price, bn.can_be_rented, bn.sku,
            	    bn.can_be_posted, bn.post_price, bn.rent_month_price AS bundle_month_price,
            	bn.is_magazine AS bundle_is_magazine, bn.distribution_in AS magazine_circulation,
            	(
            		SELECT array_to_json(array_agg(d)) AS book_ids
            		FROM (
            			SELECT b.id
            			FROM books_lomdaat b 
            			LEFT JOIN books_bundles_books_lomdaat_rel bbblr
            				ON b.id = bbblr.books_lomdaat_id
            			WHERE bbblr.books_bundles_id = bn.id
            		) d
            	)
            FROM books_bundles bn
            WHERE bn.write_date > to_timestamp(?) OR bn.sku = ANY(?)
            ORDER BY bn.id ASC
        """.trimIndent()
    }
}

inline fun <reified T> ResultSet.toFlow(crossinline mapper: suspend (RowData) -> T): Flow<T> = flow {
    forEach { emit(mapper(it)) }
}