package postgresResponses

import com.github.jasync.sql.db.ResultSet
import java.math.BigDecimal

interface PostgresRepository {
    suspend fun getBooksList(update_at: Long, updated_licenses: List<BigDecimal?>): ResultSet?
    suspend fun getBundlesList(update_at: Long, updated_licenses: List<BigDecimal?>): ResultSet?
}