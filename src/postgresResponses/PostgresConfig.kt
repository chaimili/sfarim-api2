package com.lomdaat.postgresResponses

import io.micronaut.context.annotation.ConfigurationProperties

@ConfigurationProperties("postgres")
interface PostgresConfig {
    val user: String
    val pass: String
    val db: String
    val port: String
    val host: String
}