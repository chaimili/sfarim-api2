package com.lomdaat.configs

import io.micronaut.context.annotation.ConfigurationProperties

@ConfigurationProperties("api")
interface ApiConfig {
    val user: String
    val pass: String
}