package com.lomdaat.configs

import io.micronaut.context.annotation.ConfigurationProperties

@ConfigurationProperties("folder-prefix")
interface FolderUrlPrefixConfig {
    val book: String
    val bundle: String
    val preview: String
    val cover: String
    val queue: String
}