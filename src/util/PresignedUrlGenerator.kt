package util

import com.amazonaws.services.s3.AmazonS3
import com.lomdaat.SignedUrlType
import java.util.*

class PresignedUrlGenerator(
	val s3Client: AmazonS3,
	val bucketName: String,
	val expirationInHours: Int,
	val bookPrefix: String,
	val previewPrefix: String
) {
	private fun getExpiration(hours: Int): Date {
		val expiration = java.util.Date()
		var expTimeMillis = expiration.time
		expTimeMillis += (hours * 1000 * 60 * 60).toLong()
		expiration.time = expTimeMillis
		return expiration
	}

	private fun String.addPrefix(type: SignedUrlType) = when(type) {
		SignedUrlType.book -> "$bookPrefix$this"
		SignedUrlType.preview -> "$previewPrefix$this"
	}

	fun generatePresignedUrl(type: SignedUrlType, key: String) =
		s3Client.generatePresignedUrl(bucketName, key.addPrefix(type), getExpiration(expirationInHours)).toExternalForm() ?: ""
}