package util

import com.squareup.moshi.JsonQualifier
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import mu.KotlinLogging
import java.util.*

@Retention(AnnotationRetention.RUNTIME)
@JsonQualifier
//@Target(AnnotationTarget.FIELD)
annotation class OdooFalseString

@Retention(AnnotationRetention.RUNTIME)
@JsonQualifier
annotation class OdooFalseInt

@Retention(AnnotationRetention.RUNTIME)
@JsonQualifier
annotation class OdooFalseDouble

@Retention(AnnotationRetention.RUNTIME)
@JsonQualifier
annotation class OdooFalseDate

@Retention(AnnotationRetention.RUNTIME)
@JsonQualifier
annotation class OdooIdsParser

@Retention(AnnotationRetention.RUNTIME)
@JsonQualifier
annotation class OdooIdsSecondParser

@Retention(AnnotationRetention.RUNTIME)
@JsonQualifier
annotation class OdooObjectToList

internal class StringAdapter {

    @FromJson
    @OdooFalseString
    fun fromJson(value: Any) = when (value) {
        is Double -> value.toString()
        is Boolean -> ""
        is Int -> value.toString()
        is String -> value
        else -> throw IllegalStateException("Non-object-non-boolean value for @OdooFalseString field $value")
    }

    @ToJson
    fun toJson(@OdooFalseString value: String) = if (value.isBlank()) "" else value
}

internal class IntAdapter {

    @FromJson
    @OdooFalseInt
    fun fromJson(value: Any) = when (value) {
        is Double -> value.toInt()
        is Boolean -> 0
        is Int -> value
        is String -> if (value.isNotEmpty()) value.toInt() else 0
        else -> throw IllegalStateException("Non-object-non-boolean value for @OdooFalseInt field $value")
    }

    @ToJson
    fun toJson(@OdooFalseInt value: Int) = if (value < 0) 0 else value
}

internal class DoubleAdapter {

    @FromJson
    @OdooFalseDouble
    fun fromJson(value: Any) = when (value) {
        is Double -> value
        is Boolean -> 0.toDouble()
        is Int -> value.toDouble()
        is String -> if (value.isNotEmpty()) value.toDouble() else 0.toDouble()
        else -> throw IllegalStateException("Non-object-non-boolean value for @OdooFalseDouble field $value")
    }

    @ToJson
    fun toJson(@OdooFalseDouble value: Double) = value
}

internal class DateAdapter {

    @FromJson
    @OdooFalseDate
    fun fromJson(value: Any) = when (value) {
        is Date -> ""
//        is Double -> value.toInt()
        is Boolean -> ""
//        is Int -> value
        is String -> value
        else -> throw IllegalStateException("Non-object-non-boolean value for @OdooFalseDate field $value")
    }

    @ToJson
    fun toJson(@OdooFalseDate value: String) = value
}

internal class IdAdapter {
    private val logger = KotlinLogging.logger {}

    @FromJson
    @OdooIdsParser
    fun fromJson(value: Any) = when (value) {
        is Boolean -> 0
        is Double -> value.toString().toInt()
        is ArrayList<*> -> checkArrayValue(value.toArray()[0])
        else -> throw IllegalStateException("Non-object-non-boolean value for @OdooIdsParser field $value")
    }

    @ToJson
    fun toJson(@OdooIdsParser value: Int) = if (value < 0) 0 else value

    private fun checkArrayValue(value: Any): Int =
            if (value is Boolean) 0.also { logger.debug("value not correct") }
            else (value as Double).toInt()

}

internal class IdSecondAdapter {
    private val logger = KotlinLogging.logger {}

    @FromJson
    @OdooIdsSecondParser
    fun fromJson(value: Any) = when (value) {
        is Boolean -> ""
        is Double -> value.toString()
        is ArrayList<*> -> checkArrayValue(value.toArray()[1])
        else -> throw IllegalStateException("Non-object-non-boolean value for @OdooIdsSecondParser field $value")
    }

    @ToJson
    fun toJson(@OdooIdsSecondParser value: String) = value

    private fun checkArrayValue(value: Any): String =
            if (value is Boolean) "".also { logger.debug("value not correct") }
            else
                if (value is Int)
                    value.toString()
                else
                    value.toString()

}

internal class BeginObjectAdapter {

    @FromJson
    @OdooObjectToList
    fun fromJson(value: Any) = when (value) {
//        is Boolean -> emptyList<Any>()
        is Object -> value as List<*>
        is List<*> -> value
        else -> throw IllegalStateException("Non-object-non-boolean value for @OdooObjectToList field $value")
    }

    @ToJson
    fun toJson(@OdooObjectToList value: List<Any>) = value

    private fun changeToListOfInt(value: List<*>): List<*> =
            value.map { if (it is Double) it.toInt() }.toList()
}