package com.lomdaat.util

import com.amazonaws.protocol.json.JsonContent
import com.squareup.moshi.Moshi
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.content.TextContent
import io.ktor.features.ContentConverter
import io.ktor.features.ContentNegotiation
import io.ktor.features.suitableCharset
import io.ktor.http.ContentType
import io.ktor.http.withCharset
import io.ktor.request.ApplicationReceiveRequest
import io.ktor.util.pipeline.PipelineContext
import kotlinx.coroutines.io.ByteReadChannel
import kotlinx.coroutines.io.jvm.javaio.toInputStream
import okio.Okio

class MoshiConverter(private val moshi: Moshi = Moshi.Builder().build()) : ContentConverter {
    override suspend fun convertForSend(context: PipelineContext<Any, ApplicationCall>, contentType: ContentType, value: Any): Any? {
        return TextContent(moshi.adapter(value.javaClass).toJson(value), contentType.withCharset(context.call.suitableCharset()))
    }

    override suspend fun convertForReceive(context: PipelineContext<ApplicationReceiveRequest, ApplicationCall>): Any? {
        val request = context.subject
        val channel = request.value as? ByteReadChannel ?: return null
        val reader = Okio.buffer(Okio.source(channel.toInputStream()))
        val type = request.type
        return moshi.adapter<Any>(type.java).fromJson(reader)
    }
}

fun ContentNegotiation.Configuration.moshi(block: Moshi.Builder.() -> Unit): Moshi {
    val builder = Moshi.Builder()
    builder.apply(block)
    val moshi = builder.build()
    val converter = MoshiConverter(moshi)
    register(ContentType.Application.Json, converter)
    return moshi
}