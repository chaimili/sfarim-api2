package com.lomdaat.licenseClient

import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Post
import io.micronaut.http.client.annotation.Client
import model.data.AccountLicenseStatus
import model.data.DeliveryPurchase
import model.data.ProductPurchase
import javax.validation.constraints.NotBlank

@Client("licenseApi")
interface LicenseClient {

    @Get(value = "/account/{lomdaat_account_id}/product/{content_skus}", processes = [MediaType.APPLICATION_JSON])
    fun getProductsListLicensesStatus(
            @NotBlank lomdaat_account_id: Int,
            @NotBlank content_skus: List<String>
    ): HttpResponse<List<AccountLicenseStatus>>

    @Get("/account/{lomdaat_account_id}{?last_updated}")
    fun getUpdatedAccountLicenses(@NotBlank lomdaat_account_id: Int, last_updated: Long): HttpResponse<String>
}