package model.data

data class CouponInfo(
        val status: Int,
        val name: String,
        val description: String,
        val content: Int,
        val type: Int,
        val days: Int
//        val couponCode: Int
)