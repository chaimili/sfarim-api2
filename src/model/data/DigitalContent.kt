package model.data

data class DigitalContent(
        val id: Int,
        val title: String,
        val author: String? = null,
        val active: Boolean? = true,
        val price: Double? = 0.0,
        val discount_percent: Double? = 0.0,
        val copyright: String? = null,
        val is_subscribe: Boolean? = false,
        val summary: String? = null,
        val priority: Int? = 0,
        val create_date: Long? = null,
        val write_date: Long?,
        val can_be_rented: Boolean? = false,
        val can_be_posted: Boolean? = false,
        val post_price: Double? = 0.0,
        val publication_date:String? = null,
        val edition: Int? = 0,
        val pages_count: Int? = 0,
        val rate_sum: Int? = 0,
        val raters: Int? = 0,
        val product_number: Long?,
        val type: String? = null,
        val availability: String? = null,
        val country: String? = null,
        val currency: String? = null,
        val format: String? = null,
        val category: String? = null,
        val language: String? = null,
        val book_tags: List<Tag>?,
        val status: String? = null,
        val expires_at: Long? = null
)

data class DigitalContents(
        val update_at: Long,
        val last_updated: Long,
        val digitalContents: List<DigitalContent> = emptyList()
)

data class BookId(
        val id: Int
)