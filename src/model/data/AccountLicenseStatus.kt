package model.data

data class AccountLicenseStatus(
        val sku: String,
        val expiresAt: Long? = null,
        val status: String? = null
)