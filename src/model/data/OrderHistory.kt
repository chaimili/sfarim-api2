package model.data

data class OrderHistory(
        val orderHistoryItemList: List<OrderHistoryItem> = emptyList(),
        val last_updated: Long = 0.toLong(),
        val id: Int = 0
)

data class OrderHistoryItem(
        val date: Long = 0.toLong(),
        val expires: String = "",
        val price: Double = 0.toDouble(),
        val license: String = "",
        val type: String = "",
        val cc: String = ""
)