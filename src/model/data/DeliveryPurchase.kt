package model.data

data class DeliveryPurchase (
        val customer_id: Int = 0,
        val product_id: Int,
        val name: String,
        val price: Float,
        val discount_percentage: Int,
        val payments: Int,
        val address: String,
        val is_bundle: Boolean,
        val cc_number: String?,
        val text_expiration: String?,
        val cvv: String?,
        val cc_id: Int?,
        val last_digits: String?,
        val password: String?,
        val save_for_reuse: Boolean = false
)