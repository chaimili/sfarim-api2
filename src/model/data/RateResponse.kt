package model.data

data class RateResponse(
        val id: String,
        val rate: Int
)