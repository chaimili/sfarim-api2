package model.data

data class ProductPurchase (
        val customer_id: Int = 0,
        val lomdaat_account_id: Int = 0,
        val product_sku: String,
        val name: String,
        val price: Float,
        val discount_percentage: Int,
        val payments: Int,
        val is_bundle: Boolean,
        val content_bundle_list: List<String>?,
        val days: Int?,
        val cc_number: String?,
        val text_expiration: String?,
        val cvv: String?,
        val cc_id: Int?,
        val last_digits: String?,
        val password: String?,
        val save_for_reuse: Boolean = false
)