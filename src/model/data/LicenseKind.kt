package model.data

data class LicenseKind(
        val id: Int,
        val name: String,
        val percentage: Double,
        val days: Int
)

data class LicenseKinds(
        val updated_at: Long,
        val last_updated: Long,
        val license_kinds: List<LicenseKind>
)