package model.data

data class Tag(
     val name: String
)