package model.data

data class Customer(
        val zehut: String,
        val firstname: String,
        val lastname: String,
        val mobile_number1: String,
        val cli: String,
        val email: String,
        val street: String,
        val house: Int,
        val city: String,
        val password: String,
        val creditCard: CreditCard
)