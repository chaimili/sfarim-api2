package model.data

data class CreditCard(
        val cvv: String,
        val text_expiration: String,
        val cc_id: String,
        val token: String
)