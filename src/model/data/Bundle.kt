package model.data

data class Bundle(
        val id: Int,
        val name: String?,
        val active: Boolean? = true,
        val price: Double?,
        val can_be_rented: Boolean?,
        val sku: String?,
        val can_be_posted: Boolean?,
        val post_price: Double?,
        val bundle_month_price: Double,
        val bundle_is_magazine: Boolean? = false,
        val magazine_circulation: String?,
        val book_ids: List<BookId>? = null,
        val status: String? = null,
        val expires_at: Long? = null
)