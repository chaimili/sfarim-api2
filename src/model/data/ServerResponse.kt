package model.data

data class ServerResponse(
        val id: String,
        val message: Any,
        val customer_id: Int = 0
)