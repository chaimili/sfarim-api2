package com.lomdaat.auth

import io.micronaut.context.annotation.ConfigurationProperties

@ConfigurationProperties("keycloak")
interface KeycloakConfig {
    val baseUrl: String
    val realm: String
}