package auth

import io.ktor.application.ApplicationCall
import io.ktor.auth.authentication
import io.ktor.auth.jwt.JWTPrincipal

fun ApplicationCall.getLomdaatAccountId(): Int = runCatching {
    authentication.principal<JWTPrincipal>()!!.payload.let {
        it.getClaim("lomdaat_account_id").asInt()
    }
}.getOrThrow()

fun ApplicationCall.getCustomerId(): Int = runCatching {
    authentication.principal<JWTPrincipal>()!!.payload.let {
        it.getClaim("customer_id").asInt()
    }
}.getOrThrow()

