package com.lomdaat.controllers

import com.lomdaat.kodein
import com.lomdaat.logger
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.util.getValue
import kotlinx.coroutines.launch
import model.UploadController
import model.data.ServerResponse
import org.kodein.di.generic.instance

private val uploadController: UploadController by kodein.instance<UploadController>()
fun Route.upload() {
    get("/process_book/{serial}") {
        logger.debug{"process book"}
        call.respond(HttpStatusCode.OK, ServerResponse("200", ""))
        val serial: String by call.parameters
        launch {
            uploadController.handleUpload(serial)
        }.join()
    }
}

fun Route.queueUpload(){
    get("/process_queue"){
        call.respond(HttpStatusCode.OK, ServerResponse("200", ""))
        launch {
            uploadController.handleQueueUpload()
        }.join()
    }
}

fun Route.uploadEpub(){
    get("/process_epub_book/{serial}") {
        call.respond(HttpStatusCode.OK, ServerResponse("200", ""))
        val serial: String by call.parameters
        launch {
            uploadController.handleEpubUpload(serial)
        }.join()
    }
}