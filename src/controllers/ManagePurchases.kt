package com.lomdaat.controllers

import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.locations.*
import io.ktor.response.*
import io.ktor.routing.*
import model.data.DigitalContents
import model.data.OrderHistory
import java.time.Instant
import java.util.*

@Location("/api/customer_purchase_list/{customerId}")
data class GetCustomerPurchaseList(val customerId: String, val update_at: Long)

@Location("/api/customer_digital_purchases/{id}")
data class GetCustomerDigitalPurchases(val id: Int)

fun Route.purchases() {
    authenticate {
        get<GetCustomerPurchaseList> { customer ->
            call.respond(DigitalContents(Date.from(Instant.now()).time, customer.update_at))
        }
    }

    authenticate {
        get<GetCustomerDigitalPurchases> {
            call.respond(OrderHistory(last_updated = Date.from(Instant.now()).time))
        }
    }
}