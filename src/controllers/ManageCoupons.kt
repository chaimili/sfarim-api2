package controllers

import com.lomdaat.OdooCouponInfo
import com.lomdaat.kodein
import io.ktor.application.call
import io.ktor.auth.authenticate
import io.ktor.http.HttpStatusCode
import io.ktor.locations.Location
import io.ktor.locations.post
import io.ktor.response.respond
import io.ktor.routing.Route
import model.CouponRepository
import model.data.CouponInfo
import model.data.ServerResponse
import org.kodein.di.generic.instance

const val INVALID_PASSWORD = 0
const val INVALID_CUSTOMER_ID = -1
const val INVALID_BUNDLE_ID = -2
const val INVALID_UUID = -3
const val COUPON_IN_USED = -4

enum class CouponStatus(val status: Int) {
        ACTIVE(1), INACTIVE(2), NOTFOUND(3)
    }

@Location("/api/coupon_buy")
data class PostCouponBuy(val uuid: String, val password: String, val customer_id: Int)

@Location("/api/coupon_info")
data class PostCouponInfo(val uuid: String)

private val couponRepository: CouponRepository by kodein.instance<CouponRepository>()

fun Route.coupons() {
    authenticate {
        post<PostCouponBuy> {
            val result = couponRepository.checkBookCoupon(it.uuid, it.customer_id, it.password)
            when (result) {
                COUPON_IN_USED -> call.respond(HttpStatusCode.OK, ServerResponse("400", "Coupon inactivate"))
                INVALID_CUSTOMER_ID -> call.respond(HttpStatusCode.OK, ServerResponse("109", "Invalid customerID"))
                INVALID_PASSWORD -> call.respond(HttpStatusCode.OK, ServerResponse("111", "Invalid password"))
                INVALID_UUID -> call.respond(HttpStatusCode.OK, ServerResponse("300", "Invalid UUID"))
                else -> call.respond(HttpStatusCode.OK, ServerResponse("200", "Coupon success"))
            }
        }
    }

    authenticate {
        post<PostCouponInfo> {
            val result = couponRepository.getCouponInfo(it.uuid)
            when (result.status) {
                1 -> call.respond(HttpStatusCode.OK, returnCouponInfo(result.couponInfo!!, CouponStatus.ACTIVE.status))
                2 -> call.respond(HttpStatusCode.OK, returnCouponInfo(result.couponInfo!!, CouponStatus.INACTIVE.status))
                3 -> call.respond(HttpStatusCode.BadRequest, ServerResponse("status", CouponStatus.NOTFOUND.status))
                else -> call.respond(HttpStatusCode.BadRequest, ServerResponse("status", CouponStatus.NOTFOUND.status))
            }
        }
    }
}

/**
* Convert [odooCouponInfo] to [CouponInfo]
 */

private fun returnCouponInfo(odooCouponInfo: OdooCouponInfo, status: Int) =
        CouponInfo(
                status,
                odooCouponInfo.name,
                odooCouponInfo.description,
                if(odooCouponInfo.book_id > 0) odooCouponInfo.book_id else odooCouponInfo.book_bundle_id,
                if(odooCouponInfo.book_id > 0) 1 else 2,
                odooCouponInfo.days
        )