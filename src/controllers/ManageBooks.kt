package controllers

import auth.getCustomerId
import com.github.jasync.sql.db.RowData
import com.lomdaat.model.SfarimRepository
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import io.ktor.application.call
import io.ktor.auth.authenticate
import io.ktor.http.HttpStatusCode
import io.ktor.locations.Location
import io.ktor.locations.get
import io.ktor.locations.post
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.get
import kotlinx.coroutines.flow.toList
import postgresResponses.PostgresRepository
import model.data.*
import postgresResponses.toFlow
import org.kodein.di.generic.instance
import com.lomdaat.licenseClient.LicenseClient
import auth.getLomdaatAccountId
import com.lomdaat.convertToDate
import com.lomdaat.kodein
import com.lomdaat.logger
import java.math.BigDecimal
import java.time.Instant
import java.util.*

const val DIGITAL_CONTENT_NOT_FOUND = -1
const val CANT_SAVE_RATE_TO_BOOK = -2

@Location("/api/last_updated")
data class GetLastUpdate(val type: String, val action_id: Int)

@Location("/api/un_renewable_magazine")
data class PostUnRenewableMagazine(val bundle_id: Int)

@Location("/api/rate_digital_content")
data class PostRateDigitalContent(val serial: Long, val rate: Int)

@Location("/api/image_thumbnail")
data class PostImageThumbnail(val resolution: String)

@Location("/api/test")
data class Test(val lastDigits: String)

private val sfarimRepository: SfarimRepository by kodein.instance<SfarimRepository>()
private val postgresRepository: PostgresRepository by kodein.instance<PostgresRepository>()
private val licenseClient: LicenseClient by kodein.instance<LicenseClient>()
private val moshi: Moshi by kodein.instance<Moshi>()

fun Route.bookList() {

    authenticate {
        get("/api/customer_book_list") {
            val lomdaatAccountId = call.getLomdaatAccountId()
            val updateAt = call.parameters["update_at"]!!.toLong()

            logger.debug { "book_list accountId:$lomdaatAccountId, updateAt:$updateAt" }

            val updatedLicenses = licenseClient
                .getUpdatedAccountLicenses(lomdaatAccountId, updateAt).body()
                ?.fromJsonToList<String>()?.map {
                    it.toBigDecimalOrNull()
                }

            logger.debug { "book_list updatedLicenses:$updatedLicenses" }

            val result = postgresRepository
                .getBooksList(updateAt, updatedLicenses ?: emptyList())
                ?.toFlow { it.toDigitalContent() }?.toList()


            logger.debug { "book_list result:$result" }

            if (!result.isNullOrEmpty()) {
                val license = licenseClient.getProductsListLicensesStatus(
                    lomdaatAccountId,
                    result.map { it.product_number?.toString() ?: "" }
                ).body()

                logger.debug { "book_list license:$license" }

                val json = result.map { book ->
                    book.copy(
                        status = license?.singleOrNull { it.sku == book.product_number.toString() }?.status,
                        expires_at = license?.singleOrNull { it.sku == book.product_number.toString() }?.expiresAt
                    )
                }.fromListToJson()

                logger.debug { "book_list json:$json" }

                call.respond(HttpStatusCode.OK, listOfBooks2(json))
            } else
                call.respond(HttpStatusCode.OK, listOfBooks2(null))
        }
    }

    authenticate {
        get("/api/customer_bundle_list") {
            val lomdaatAccountId = call.getLomdaatAccountId()
            val updateAt = call.parameters["update_at"]!!.toLong()

            val updatedLicenses =
                licenseClient.getUpdatedAccountLicenses(lomdaatAccountId, updateAt).body()
                    ?.fromJsonToList<String>()?.map {
                        it.toBigDecimalOrNull()
                    }

            val result = postgresRepository
                .getBundlesList(updateAt, updatedLicenses ?: emptyList())
                ?.toFlow { it.toBundle() }?.toList()

            if (!result.isNullOrEmpty()) {
                val license = licenseClient.getProductsListLicensesStatus(
                    lomdaatAccountId,
                    result.map { it.sku ?: "" }
                ).body()

                val json = result.map { bundle ->
                    bundle.copy(
                        status = license?.singleOrNull { it.sku == bundle.sku }?.status,
                        expires_at = license?.singleOrNull { it.sku == bundle.sku }?.expiresAt
                    )
                }.fromListToJson()

                call.respond(HttpStatusCode.OK, listOfBundle2(json))
            } else
                call.respond(HttpStatusCode.OK, listOfBundle2(null))
        }
    }

    authenticate {
        post<PostUnRenewableMagazine> {
            when (sfarimRepository.getUnRenewableMagazine(call.getCustomerId(), it.bundle_id)) {
                INVALID_CUSTOMER_ID -> call.respond(
                    HttpStatusCode.OK,
                    ServerResponse("109", "Invalid Customer ID")
                )
                INVALID_BUNDLE_ID -> call.respond(
                    HttpStatusCode.OK,
                    ServerResponse("110", "Invalid bundle ID")
                )
                1 -> call.respond(HttpStatusCode.OK, ServerResponse("200", ""))
                else -> call.respond(HttpStatusCode.OK, ServerResponse("400", ""))
            }
        }
    }

    authenticate {
        post<PostRateDigitalContent> {
            when (val result = sfarimRepository.updateRateOfBook(it.serial, it.rate)) {
                DIGITAL_CONTENT_NOT_FOUND -> call.respond(
                    HttpStatusCode.OK,
                    ServerResponse("110", "digital content not found")
                )
                CANT_SAVE_RATE_TO_BOOK -> call.respond(
                    HttpStatusCode.OK,
                    ServerResponse("120", "cant save rate to book")
                )
                else -> call.respond(HttpStatusCode.OK, RateResponse("200", result))
            }
        }
    }

    authenticate {
        get<Test> {
            val result = sfarimRepository.getLastDigits(call.getCustomerId(), it.lastDigits)
            if (result != null) {
                call.respond(HttpStatusCode.OK, result)
            } else
                call.respond(HttpStatusCode.NotFound)
        }
    }

    authenticate {
        post<PostImageThumbnail> {
            //TODO: change this api
            val serial = if (call.parameters["serial"] != null)
                call.parameters["serial"]!!
            else
                call.parameters["id"]!!

            call.respond("ImageToShow $serial")
        }
    }

    authenticate {
        get<GetLastUpdate> {
            if (it.type == "book") {
                val result = sfarimRepository.getLastUpdatedBook()
                if (result != null)
                    call.respond(HttpStatusCode.OK, result)
                else
                    call.respond(HttpStatusCode.NotFound)
            } else
                call.respond(HttpStatusCode.NotFound)
        }
    }
}

private fun RowData.toDigitalContent() = DigitalContent(
    id = getAs("id"), title = getAs("title"),
    author = getAs("author"), active = getAs("active"), price = getDouble("price"),
    discount_percent = getDouble("discount_percent"), copyright = getAs("copyright"),
    is_subscribe = getAs("is_subscribe"), summary = getAs("summary"),
    priority = getAs("priority"), create_date = getAs("create_date"),
    write_date = getAs("write_date"), can_be_rented = getAs("can_be_rented"),
    can_be_posted = getAs("can_be_posted"), post_price = getDouble("post_price"),
    publication_date = getAs("publication_date"), edition = getAs("edition"),
    pages_count = getAs("pages_count"), rate_sum = getAs("rate_sum"),
    raters = getAs("raters"), product_number = getAs<BigDecimal>("product_number").longValueExact(),
    type = getAs("type"), availability = getAs("availability"), country = getAs("country"),
    currency = getAs("currency"), format = getAs("format"), category = getAs("category"),
    language = getAs("language"), book_tags = getAs("book_tags")
)

private fun RowData.toBundle() = Bundle(
    id = getAs("id"), name = getAs("name"), active = getAs("active"),
    price = getAs("price"), can_be_rented = getAs("can_be_rented"), sku = getAs("sku"),
    can_be_posted = getAs("can_be_posted"), post_price = getAs("post_price"),
    bundle_month_price = getAs("bundle_month_price"), bundle_is_magazine = getAs("bundle_is_magazine"),
    magazine_circulation = getAs("magazine_circulation"),
    book_ids = getAs<String?>("book_ids")?.fromJsonToList()
)

private inline fun <reified T> String?.fromJsonToList(): List<T> {
    val listType = Types.newParameterizedType(List::class.java, T::class.java)
    val adapter = moshi.adapter<List<T>>(listType)
    return adapter.fromJson(this ?: "") ?: emptyList()
}

private inline fun <reified T> List<T>.fromListToJson(): String {
    val listType = Types.newParameterizedType(List::class.java, T::class.java)
    val adapter = moshi.adapter<List<T>>(listType)
    return adapter.toJson(this)
}


/**
 * Create response to listOfBooks in json
 */
private suspend fun listOfBooks2(result: String?) =
    ("{\"update_at\": ${Date.from(Instant.now()).time / 1000}," +
            " \"last_updated\": ${convertToDate(sfarimRepository.getLastUpdatedBook()!!.write_date).time / 1000}," +
            " \"digitalContents\": ")
        .plus(result?.plus("}") ?: "[]}")

/**
 * Create response to listOfBundles in json
 */
private suspend fun listOfBundle2(result: String?) =
    ("{\"update_at\": ${Date.from(Instant.now()).time / 1000}," +
            " \"last_updated\": ${convertToDate(sfarimRepository.getLastUpdatedBook()!!.write_date).time / 1000}," +
            " \"bundles\": ")
        .plus(result?.plus("}") ?: "[]}")