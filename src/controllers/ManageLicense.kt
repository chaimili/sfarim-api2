package controllers

import com.lomdaat.kodein
import io.ktor.application.call
import io.ktor.auth.authenticate
import io.ktor.http.HttpStatusCode
import io.ktor.locations.Location
import io.ktor.locations.get
import io.ktor.response.respond
import io.ktor.routing.Route
import model.LicenseRepository
import model.data.LicenseKind
import model.data.LicenseKinds
import model.data.ServerResponse
import odooResponses.OdooLicenseKinds
import org.kodein.di.generic.instance

@Location("/api/license_kinds")
data class GetLicenseKinds(val updated_at: Long)

private val licenseRepository: LicenseRepository by kodein.instance<LicenseRepository>()

fun Route.licenses(){
    authenticate {
        get<GetLicenseKinds> {
            if (it.updated_at > 0) {
                val result = licenseRepository.getLicenseKinds(it.updated_at)
                call.respond(HttpStatusCode.OK, returnLicenseKindsResponse(result, it.updated_at))
            }
            else
                call.respond(HttpStatusCode.OK, ServerResponse("100", "updated_at should be bigger then 0"))
        }
    }

}

private fun returnLicenseKindsResponse(result: OdooLicenseKinds, updated_at: Long) =
        LicenseKinds(
                updated_at,
                result.last_updated.time / 1000,
                result.odooLicenseKind.map { LicenseKind(
                        it.id,
                        it.name,
                        it.precentage,
                        it.days
                ) }
        )