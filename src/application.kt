package com.lomdaat

import com.auth0.jwk.JwkProvider
import com.github.jasync.sql.db.Connection
import com.lomdaat.controllers.purchases
import com.lomdaat.controllers.queueUpload
import com.lomdaat.controllers.upload
import com.lomdaat.controllers.uploadEpub
import com.lomdaat.util.MoshiConverter
import controllers.licenses
import com.squareup.moshi.Moshi
import controllers.bookList
import controllers.coupons
import io.ktor.application.*
import io.ktor.response.*
import io.ktor.request.*
import io.ktor.routing.*
import io.ktor.http.*
import io.ktor.features.*
import org.slf4j.event.*
import io.ktor.auth.*
import io.ktor.auth.jwt.JWTPrincipal
import io.ktor.auth.jwt.jwt
import io.ktor.locations.*
import io.ktor.server.engine.applicationEngineEnvironment
import io.ktor.server.engine.connector
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import org.kodein.di.direct
import org.kodein.di.generic.instance
import postgresResponses.DBConnectivityException
import util.PresignedUrlGenerator
import java.text.SimpleDateFormat
import java.util.*
import io.ktor.util.getValue
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

fun main() {
    val env = applicationEngineEnvironment {
        module {
            main()
        }
        // Private API
        connector {
            host = "0.0.0.0"
            port = 9090
        }
        // Public API
        connector {
            host = "0.0.0.0"
            port = 8080
        }
    }
    embeddedServer(Netty, env).start(true)
}

class AuthenticationException : RuntimeException()
class AuthorizationException : RuntimeException()

enum class SignedUrlType { book, preview }

@Location("/api/{type}/signed_url")
data class SignedUrl(val key: String, val type: SignedUrlType)

data class LinkResponse(val url: String)

data class TimeResponse(val datetime: String)

fun Application.main() {
    //for PDFBox in java 8
    System.setProperty("sun.java2d.cmm", "sun.java2d.cmm.kcms.KcmsServiceProvider")
    System.setProperty("-Dorg.apache.pdfbox.rendering.UsePureJavaCMYKConversion", "true")

    val moshi by kodein.instance<Moshi>()
    val presignedUrlGenerator by kodein.instance<PresignedUrlGenerator>()
    val urlPrefixes by kodein.instance<UrlPrefixes>()

    val keycloakRealm by kodein.instance<String>("jwkRealm")
    val jwkIssuer by kodein.instance<String>("jwkIssuer")
    val jwkProvider by kodein.instance<JwkProvider>()

    Runtime.getRuntime().addShutdownHook(object : Thread() {
        override fun run() {
            kodein.direct.instance<Connection>().disconnect().get()
        }
    })

    install(CallLogging) {
        level = Level.INFO
        filter { call -> call.request.path().startsWith("/") }
    }

    install(DefaultHeaders) {
        header("X-Engine", "Ktor") // will send this header with each response
    }

    install(Authentication) {
        jwt {
            verifier(jwkProvider, jwkIssuer)
            realm = keycloakRealm
            validate { credentials ->
                JWTPrincipal(credentials.payload)
            }
        }
    }

    install(ContentNegotiation) {
        register(ContentType.Application.Json, MoshiConverter(moshi))
    }

    install(Locations) {
    }

    install(StatusPages) {
        this.exception<DBConnectivityException> {
            call.respond(HttpStatusCode.ServiceUnavailable, "Failed Connection DB")
        }
    }

    routing {
        trace { application.log.trace(it.buildText()) }
        port(8080) {

            install(StatusPages) {
                exception<AuthenticationException> { cause ->
                    call.respond(HttpStatusCode.Unauthorized)
                }
                exception<AuthorizationException> { cause ->
                    call.respond(HttpStatusCode.Forbidden)
                }
            }


            authenticate {
                // Make signed url to key of digital content
                get<SignedUrl> {
                    val signedUrl = presignedUrlGenerator.generatePresignedUrl(it.type, it.key)

                    call.respond(LinkResponse(signedUrl))
                }

                get("/api/url_prefix/{type}") {
                    val type: String by call.parameters

                    val prefix = when (type) {
                        "book" -> urlPrefixes.coverUrlPrefix
                        "bundle" -> urlPrefixes.bundleUrlPrefix
                        "preview" -> urlPrefixes.previewUrlPrefix
                        else -> null
                    }

                    if (prefix != null)
                        call.respond(LinkResponse(prefix))
                    else
                        call.respond(HttpStatusCode.NotFound)
                }

                get("/api/current_timestamp") {
                    val timestamp = LocalDateTime.now()
                    call.respond(HttpStatusCode.OK, TimeResponse(timestamp.format(formatter)))
                }
            }

            purchases()

            bookList()

            coupons()

            licenses()
        }
        // Internal port
        port(9090) {
            upload()

            queueUpload()

            uploadEpub()
        }
    }
}

/**
 * Date Converter
 */
val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")!!
val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

fun convertToDate(string: String): Date = format.parse(string)

fun convertUnixTimeToDate(time2: Long): Date {
    val date = Date().apply { time = time2 * 1000 }
    return convertToDate(format.format(date))
}

fun convertDateToString(time: Long): String =
        SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Date(time))
