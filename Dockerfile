FROM openjdk:8-jre-alpine

COPY ./build/libs/sfarim-api-0.0.1-all.jar /root/sfarim-api-0.0.1-all.jar

WORKDIR /root

ENV ODOO_HOST=core_odoo ODOO_PORT=8069 ODOO_DB=koshernet

ENV ODOO_USER, ODOO_PASS, S3_ENDPOINT, S3_ACCESS-KEY, S3_SECRET-KEY, S3_BUCKET

ENV QUEUE_FOLDER_PREFIX=book_queue/ BOOK_FOLDER_PREFIX=book_files/ PREVIEW_FOLDER_PREFIX=book_summaries/ COVER_FOLDER_PREFIX=book_images/ BUNDLE_FOLDER_PREFIX=bundle_images/

ENV POSTGRES_HOST, POSTGRES_PORT, POSTGRES_DB, POSTGRES_USER, POSTGRES_PASS

ENV LOGCONF=logback-prod.xml

ENV API_USER, API_PASS, GMAIL_USER, GMAIL_PASS

ENV GMAIL_HOST=smtp.gmail.com GMAIL_PORT=587

CMD ["java", "-server", "-XX:+UnlockExperimentalVMOptions", "-XX:+UseCGroupMemoryLimitForHeap", "-XX:InitialRAMFraction=2", "-XX:MinRAMFraction=2", "-XX:MaxRAMFraction=2", "-XX:+UseG1GC", "-XX:MaxGCPauseMillis=100", "-XX:+UseStringDeduplication", "-jar", "sfarim-api-0.0.1-all.jar"]